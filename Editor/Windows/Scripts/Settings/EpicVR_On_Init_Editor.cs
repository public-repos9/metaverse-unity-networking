using UnityEditor;

namespace MUN
{
    [InitializeOnLoad]
    public static class EpicVR_On_Init_Editor
    {
        private const string DASHBOARD_FIRST_INIT = "EnableDashboard_FirstInit";

        static EpicVR_On_Init_Editor() => EditorApplication.delayCall += EnableDashboard;

        private static void EnableDashboard()
        {
            if (!SessionState.GetBool(DASHBOARD_FIRST_INIT, false))
            {
                EpicVR_Dashboard.Init();
                SessionState.SetBool(DASHBOARD_FIRST_INIT, true);
            }
        }
    }
}