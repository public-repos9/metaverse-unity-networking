using MUN.Service;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using static MUN.EpicVR_Validators;

namespace MUN
{
    public class EpicVR_RegisterWindow : EditorWindow
    {
        [SerializeField] private VisualTreeAsset registerWindow;

        public static void Init()
        {
            EpicVR_RegisterWindow wnd = GetWindow<EpicVR_RegisterWindow>();
            wnd.minSize = new Vector2(EpicVR_MUN_Consts.MIN_WIN_SIZE.x, 335);
            wnd.titleContent = new GUIContent("EpicVR_RegisterWindow");
        }

        public void CreateGUI()
        {
            VisualElement root = rootVisualElement;
            registerWindow.CloneTree(root);

            //Handle PanelInfo:
            var panelInfo = root.Q<VisualElement>(name: "panel-info");
            panelInfo.style.display = DisplayStyle.None;

            var panelSuccess = root.Q<VisualElement>(name: "panel-info-success");

            //Handle Button:
            var btnRegister = root.Q<Button>(name: "btn-register");
            btnRegister.SetEnabled(false);

            //Handle Fields:
            var fieldUsername = root.Q<TextField>(name: "field-username");
            var fieldEmail = root.Q<TextField>(name: "field-email");
            var fieldPassword = root.Q<TextField>(name: "field-password");
            var fieldConfirmPassword = root.Q<TextField>(name: "field-confirm-password");

            //Handle Validators:
            var usernameValidator = fieldUsername.Q<Label>(name: "txt-validator");
            var emailValidator = fieldEmail.Q<Label>(name: "txt-validator");
            var passwordValidator = fieldPassword.Q<Label>(name: "txt-validator");
            var confirmPasswordValidator = fieldConfirmPassword.Q<Label>(name: "txt-validator");

            //Check Validation OnCreate:
            IsValid(Type.Login, in usernameValidator, false, fieldUsername.value);
            IsValid(Type.Email, in emailValidator, false, fieldEmail.value);
            IsValid(Type.Password, in passwordValidator, false, fieldPassword.value);
            IsValid(Type.ConfirmPassword, in confirmPasswordValidator, false, fieldConfirmPassword.value, fieldPassword.value);
            CheckButtonRegisterButton();

            fieldUsername.RegisterValueChangedCallback((evt) =>
            {
                IsValid(Type.Login, in usernameValidator, true, fieldUsername.value);
                CheckButtonRegisterButton();
                panelInfo.style.display = DisplayStyle.None;
            });

            fieldEmail.RegisterValueChangedCallback((evt) =>
            {
                IsValid(Type.Email, in emailValidator, true, fieldEmail.value);
                CheckButtonRegisterButton();
                panelInfo.style.display = DisplayStyle.None;
            });

            fieldPassword.RegisterValueChangedCallback((evt) =>
            {
                IsValid(Type.Password, in passwordValidator, true, fieldPassword.value);
                IsValid(Type.ConfirmPassword, in confirmPasswordValidator, true, fieldConfirmPassword.value, fieldPassword.value);
                CheckButtonRegisterButton();
                panelInfo.style.display = DisplayStyle.None;
            });

            fieldConfirmPassword.RegisterValueChangedCallback((evt) =>
            {
                IsValid(Type.Password, in passwordValidator, true, fieldPassword.value);
                IsValid(Type.ConfirmPassword, in confirmPasswordValidator, true, fieldConfirmPassword.value, fieldPassword.value);
                CheckButtonRegisterButton();
                panelInfo.style.display = DisplayStyle.None;
            });

            btnRegister.clicked += async () =>
            {
                var password = fieldPassword.value;
                var confirmPassword = fieldConfirmPassword.value;

                btnRegister.SetEnabled(false);

                bool isSuccess = await ServiceRequest.Account.Register(fieldUsername.value, fieldEmail.value, password, confirmPassword, (errors) =>
                {
                    var errMsg = string.Join("; ", errors.Errors.Values);
                    panelInfo.Q<Label>(name: "txt-info").text = errMsg;
                    panelInfo.style.display = DisplayStyle.Flex;
                });

                fieldPassword.SetValueWithoutNotify(string.Empty);
                fieldConfirmPassword.SetValueWithoutNotify(string.Empty);

                if (isSuccess)
                {
                    panelSuccess.style.display = DisplayStyle.Flex;
                    await Task.Delay(3000);
                    Close();
                    EpicVR_LoginWindow.Init();
                }
            };

            void CheckButtonRegisterButton()
            {
                btnRegister.SetEnabled(
                    !usernameValidator.visible
                    && !emailValidator.visible
                    && !passwordValidator.visible
                    && !confirmPasswordValidator.visible);
            }
        }
    }
}