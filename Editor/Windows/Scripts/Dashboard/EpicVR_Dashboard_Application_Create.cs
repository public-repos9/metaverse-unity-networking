using MUN.Service;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using static MUN.EpicVR_Validators;
using static MUN.Service.ServiceRequest;
using Type = MUN.EpicVR_Validators.Type;

namespace MUN
{
    public class EpicVR_Dashboard_Application_Create : EditorWindow
    {
        [SerializeField] private VisualTreeAsset createApplicationWindow;

        private List<Toggle> regionToggls = new List<Toggle>();

        private readonly Dictionary<int, string> tickrateExamples = new Dictionary<int, string>()
        {
            { 0, "(Slow) e.g. Card game" },
            { 10, "(Medium) e.g. Social Apps" },
            { 20, "(Fast) e.g. FPS" }
        };

        private bool AnyRegionIsEnabled => regionToggls.Any(x => x.value);
        private string[] SelectedRegions => regionToggls.Where(x => x.value).Select(x => x.label.Trim()).ToArray();

        public async Task CreateGUIAsync(VisualElement root, IOrganization selectedOrganization)
        {
            root.Clear();
            createApplicationWindow.CloneTree(root);

            #region Handle Elements

            var panelInfoErrors = root.Q<VisualElement>(name: "panel-info");
            var panelSuccess = root.Q<VisualElement>(name: "panel-info-success");

            var fieldAppName = root.Q<TextField>(name: "field-app-name");
            var validatorAppName = fieldAppName.Q<Label>(name: "txt-validator");

            var sectionRegions = root.Q<VisualElement>(name: "section-regions");
            var regionsList = sectionRegions.Q<ScrollView>(name: "regions-list");

            var maxPlayers = root.Q<SliderInt>(name: "max-players");

            var tickrate = root.Q<SliderInt>(name: "tick-rate");
            var tickrateExample = root.Q<TextField>(name: "field-tick-rate-info");

            var useVoiceChat = root.Q<Toggle>(name: "tgl-voice-chat");

            #endregion Handle Elements

            #region Init

            UpdateTickrateExample(tickrate.value);
            //Regions:
            HandleButtonCreate(out var btnCreate);
            await HandleRegionsAsync();

            #endregion Init

            #region Callbacks

            fieldAppName.RegisterValueChangedCallback((callback) =>
            {
                var isValid = IsValid(Type.ApplicationName, validatorAppName, true, fieldAppName.value);
                btnCreate.SetEnabled(isValid && AnyRegionIsEnabled);

                panelInfoErrors.style.display = DisplayStyle.None;
                panelSuccess.style.display = DisplayStyle.None;
            });

            tickrate.RegisterValueChangedCallback((callback) =>
            {
                var value = tickrate.value;
                UpdateTickrateExample(value);
            });

            btnCreate.clicked += async () =>
            {
                panelInfoErrors.style.display = DisplayStyle.None;
                panelSuccess.style.display = DisplayStyle.None;

                Debug.Log($"Selected regions: {string.Join(',', SelectedRegions)}.");

                btnCreate.SetEnabled(false);
                var createApplication = new CreateApplication
                {
                    maxPlayers = (uint)maxPlayers.value,
                    name = fieldAppName.value,
                    tickRate = (byte)tickrate.value,
                    useVoiceChat = useVoiceChat.value,
                    organizationId = selectedOrganization.Id,
                    regionNames = SelectedRegions
                };

                var isSuccess = await ServiceRequest.Application.Create(createApplication, (err) =>
                {
                    var errMsg = string.Join("; ", err.Errors.Values);
                    panelInfoErrors.Q<Label>(name: "txt-error-text").text = errMsg;
                    panelInfoErrors.style.display = DisplayStyle.Flex;
                });

                if (isSuccess)
                {
                    panelSuccess.style.display = DisplayStyle.Flex;
                    await ServiceRequest.Application.GetAll(selectedOrganization.Id);
                }
            };

            #endregion Callbacks

            async Task HandleRegionsAsync()
            {
                var regions = await ServiceRequest.Regions.GetAll();

                for (int i = 0; i < regions.Length; i++)
                {
                    var element = new Toggle("\t" + regions[i]);
                    element.style.fontSize = 14;

                    if (i == 0)
                        element.value = true;

                    regionsList.Add(element);
                    regionToggls.Add(element);

                    element.RegisterValueChangedCallback((callback) =>
                    {
                        btnCreate.SetEnabled(!validatorAppName.visible && AnyRegionIsEnabled);
                    });
                }
            }
            void HandleButtonCreate(out Button btnCreate)
            {
                btnCreate = root.Q<Button>(name: "btn-create");
                btnCreate.SetEnabled(false);

                btnCreate.clicked += () =>
                {
                    //TODO: Create app
                };
            }

            void UpdateTickrateExample(int value)
            {
                var exampleText = tickrateExamples.Last(x => x.Key < value).Value;
                tickrateExample.SetValueWithoutNotify(exampleText);
            }
        }
    }
}