using MUN.Service;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine.UIElements;
using static MUN.EpicVR_Validators;

namespace MUN
{
    public class EpicVR_Dashboard_Account : EditorWindow
    {
        public async Task CreateGuiAsync(VisualElement root)
        {
            #region Handle Elements

            //Handle PanelInfo:
            var panelInfoErrors = root.Q<VisualElement>(name: "panel-info-error");
            var panelInfoSuccess = root.Q<VisualElement>(name: "panel-info-success");
            panelInfoErrors.style.display = DisplayStyle.None;
            panelInfoSuccess.style.display = DisplayStyle.None;

            //Handle Buttons:
            var btnUpdate = root.Q<Button>(name: "btn-update");
            btnUpdate.SetEnabled(false);

            //Handle Sections:
            var sectionUpdate = root.Q<VisualElement>(name: "section-update");
            sectionUpdate.SetEnabled(false);

            //Handle Fields:
            var fieldUsername = root.Q<TextField>(name: "field-username");
            var fieldEmail = root.Q<TextField>(name: "field-email");
            var fieldRole = root.Q<TextField>(name: "field-role");
            var fieldNewPassword = root.Q<TextField>(name: "field-new-password");
            var fieldConfirmNewPassword = root.Q<TextField>(name: "field-confirm-new-password");
            var fieldCurrentPassword = root.Q<TextField>(name: "field-current-password");

            fieldRole.SetEnabled(false);

            //Handle Validators:
            var validatorUsername = fieldUsername.Q<Label>(name: "txt-validator");
            var validatorEmail = fieldEmail.Q<Label>(name: "txt-validator");
            var validatorNewPassword = fieldNewPassword.Q<Label>(name: "txt-validator");
            var validatorConfirmNewPassword = fieldConfirmNewPassword.Q<Label>(name: "txt-validator");
            var validatorCurrentPassword = fieldCurrentPassword.Q<Label>(name: "txt-validator");

            #endregion Handle Elements

            #region Handle Init Values

            if (string.IsNullOrEmpty(ServiceRequest.Account.Token)) return;
            bool hasErrors = false;

            if (ServiceRequest.Account.User is null)
            {
                await ServiceRequest.Account.Get((err) =>
                {
                    hasErrors = true;
                });

                if (hasErrors) return;
            }

            fieldUsername.value = ServiceRequest.Account.Username;
            fieldEmail.value = ServiceRequest.Account.User.Email;
            fieldRole.value = ServiceRequest.Account.User.Role;

            #endregion Handle Init Values

            #region Handle Callbacks

            fieldUsername.RegisterValueChangedCallback((evt) =>
            {
                IsValid(Type.Login, in validatorUsername, true, fieldUsername.value);
                SetSectionUpdateActive();
                panelInfoErrors.style.display = DisplayStyle.None;
                panelInfoSuccess.style.display = DisplayStyle.None;
            });

            fieldEmail.RegisterValueChangedCallback((evt) =>
            {
                IsValid(Type.Email, in validatorEmail, true, fieldEmail.value);
                SetSectionUpdateActive();
                panelInfoErrors.style.display = DisplayStyle.None;
                panelInfoSuccess.style.display = DisplayStyle.None;
            });

            fieldNewPassword.RegisterValueChangedCallback((evt) =>
            {
                if (string.IsNullOrEmpty(fieldNewPassword.value))
                {
                    validatorNewPassword.visible = false;
                    validatorConfirmNewPassword.visible = false;
                    fieldConfirmNewPassword.value = string.Empty;
                }
                else
                {
                    IsValid(Type.Password, in validatorNewPassword, true, fieldNewPassword.value);
                }

                SetSectionUpdateActive();
                panelInfoErrors.style.display = DisplayStyle.None;
                panelInfoSuccess.style.display = DisplayStyle.None;
            });

            fieldConfirmNewPassword.RegisterValueChangedCallback((evt) =>
            {
                IsValid(Type.ConfirmPassword, in validatorConfirmNewPassword, true, fieldConfirmNewPassword.value, fieldNewPassword.value);
                SetSectionUpdateActive();
                panelInfoErrors.style.display = DisplayStyle.None;
                panelInfoSuccess.style.display = DisplayStyle.None;
            });

            fieldCurrentPassword.RegisterValueChangedCallback((evt) =>
            {
                IsValid(Type.Password, in validatorCurrentPassword, true, fieldCurrentPassword.value);
                SetButtonUpdateActive();
                panelInfoErrors.style.display = DisplayStyle.None;
                panelInfoSuccess.style.display = DisplayStyle.None;
            });

            btnUpdate.clicked += async () =>
            {
                var hasErrors = false;
                btnUpdate.SetEnabled(false);

                await ServiceRequest.Account.Update(fieldUsername.value,
                    fieldEmail.value,
                    fieldCurrentPassword.value,
                    fieldNewPassword.value,
                    fieldConfirmNewPassword.value,
                    (err) =>
                    {
                        hasErrors = true;
                        var errMsg = string.Join("; ", err.Errors.Values);
                        panelInfoErrors.Q<Label>(name: "txt-info").text = errMsg;
                    });

                fieldCurrentPassword.SetValueWithoutNotify(string.Empty);
                validatorCurrentPassword.visible = false;

                if (!hasErrors)
                {
                    fieldNewPassword.value = string.Empty;
                    fieldConfirmNewPassword.value = string.Empty;
                    panelInfoSuccess.style.display = DisplayStyle.Flex;
                }
                else
                {
                    panelInfoErrors.style.display = DisplayStyle.Flex;
                }
            };

            #endregion Handle Callbacks

            void SetSectionUpdateActive()
            {
                bool currenctActive = sectionUpdate.enabledInHierarchy;
                bool isActive = !ServiceRequest.Account.Username.Equals(fieldUsername.value)
                             || !ServiceRequest.Account.User.Email.Equals(fieldEmail.value)
                             || (!string.IsNullOrEmpty(fieldNewPassword.value)
                             && !validatorNewPassword.visible
                             && IsValid(Type.ConfirmPassword, in validatorConfirmNewPassword, true, fieldConfirmNewPassword.value, fieldNewPassword.value));

                if (currenctActive != isActive)
                    fieldCurrentPassword.value = string.Empty;

                sectionUpdate.SetEnabled(isActive);
            }

            void SetButtonUpdateActive()
            {
                btnUpdate.SetEnabled(sectionUpdate.enabledSelf && !validatorCurrentPassword.visible);
            }
        }
    }
}