using MUN;
using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public class EpicVR_Accept_Acction : EditorWindow
{
    [SerializeField] private VisualTreeAsset actionAsset;

    private struct ActionData
    {
        public string header;
        public string description;
        public string expectedText;
        public Action onApply;
    }

    private static ActionData actionData;

    public static void Init(string header, string description, string expectedText, Action onApply)
    {
        actionData = new ActionData
        {
            header = header,
            description = description,
            expectedText = expectedText,
            onApply = onApply
        };

        EpicVR_Accept_Acction wnd = GetWindow<EpicVR_Accept_Acction>();
        wnd.titleContent = new GUIContent(header);
        wnd.minSize = EpicVR_MUN_Consts.MIN_WIN_SIZE;
        wnd.ShowModal();
    }

    private void CreateGUI()
    {
        // Each editor window contains a root VisualElement object
        VisualElement root = rootVisualElement;
        actionAsset.CloneTree(root);

        #region Handle Inputs

        //Labels:
        var txtHeader = root.Q<Label>(name: "txt-header");
        var txtDescription = root.Q<Label>(name: "txt-info");

        //Fields:
        var fieldValue = root.Q<TextField>(name: "field-value");

        //Buttons:
        var btnCancel = root.Q<Button>(name: "btn-cancel");
        var btnApply = root.Q<Button>(name: "btn-apply");

        #endregion Handle Inputs

        #region Init Inputs

        txtHeader.text = actionData.header;
        txtDescription.text = actionData.description;

        btnApply.SetEnabled(false);

        fieldValue.Focus();

        #endregion Init Inputs

        #region Callbacks

        fieldValue.RegisterValueChangedCallback((callbacks) =>
        {
            var isCorrectValue = actionData.expectedText.Equals(fieldValue.value);
            btnApply.SetEnabled(isCorrectValue);
        });

        btnCancel.clicked += () =>
        {
            Close();
        };

        btnApply.clicked += () =>
        {
            actionData.onApply?.Invoke();
            Close();
        };

        #endregion Callbacks
    }
}