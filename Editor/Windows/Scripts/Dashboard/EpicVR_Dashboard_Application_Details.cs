using MUN;
using MUN.Client;
using MUN.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using static MUN.Service.ServiceRequest;

public class EpicVR_Dashboard_Application_Details : EditorWindow
{
    private const string DELETE_APPLICATION_DESCRIPTION = "If you want to delete this application, you must enter its name ({0}) in the field below.";
    private const string SUCCESSFULLY_SET_APP_ID = "You have successfully set AppId in the project.";
    private const string SUCCESSFULLY_UPDATED_APP = "The application has been updated successfully.";

    [SerializeField] private VisualTreeAsset visualTree;

    private static IApplication Application;
    private static Action RepaintAppsWindow;

    private List<Toggle> regionToggls = new List<Toggle>();

    private readonly Dictionary<int, string> tickrateExamples = new Dictionary<int, string>()
        {
            { 0, "(Slow) e.g. Card game" },
            { 10, "(Medium) e.g. Social Apps" },
            { 20, "(Fast) e.g. FPS" }
        };

    private bool AnyRegionIsEnabled => regionToggls.Any(x => x.value);

    private string[] SelectedRegions => regionToggls
        .Where(x => x.value)
        .Select(x => x.label.Trim())
        .OrderBy(x => x)
        .ToArray();

    public static void Init(in IApplication showApplication, Action repaintAppsWindow)
    {
        Application = showApplication;
        RepaintAppsWindow = repaintAppsWindow;

        EpicVR_Dashboard_Application_Details wnd = GetWindow<EpicVR_Dashboard_Application_Details>();
        wnd.titleContent = new GUIContent($"Details: {Application.Name}");
        wnd.CreateGUI();
    }

    public async void CreateGUI()
    {
        VisualElement root = rootVisualElement;
        root.Clear();
        visualTree.CloneTree(root);

        #region Handle Elements

        var panelErrors = root.Q<VisualElement>(name: "panel-info");
        var panelSuccess = root.Q<VisualElement>(name: "panel-info-success");
        var loadingView = root.Q<VisualElement>(name: "loading-view");

        var fieldAppName = root.Q<TextField>(name: "field-app-name");
        var fieldAppId = root.Q<TextField>(name: "field-appid");

        var sectionRegions = root.Q<VisualElement>(name: "section-regions");
        var regionsList = sectionRegions.Q<ScrollView>(name: "regions-list");

        var maxPlayers = root.Q<SliderInt>(name: "max-players");

        var tickrate = root.Q<SliderInt>(name: "tick-rate");
        var tickrateExample = root.Q<TextField>(name: "field-tick-rate-info");

        var useVoiceChat = root.Q<Toggle>(name: "tgl-voice-chat");
        var isServerActive = root.Q<Toggle>(name: "tgl-is-server-active");

        var btnShowHideAppId = root.Q<Button>(name: "btn-show");
        var btnCopyAppId = root.Q<Button>(name: "btn-copy");
        var btnUpdate = root.Q<Button>(name: "btn-update");
        var btnDelete = root.Q<Button>(name: "btn-delete");

        #endregion Handle Elements

        #region Init

        btnUpdate.visible = false;
        fieldAppName.value = Application.Name;

        fieldAppId.isPasswordField = true;
        fieldAppId.value = Application.AppId;

        await HandleRegionsAsync();

        maxPlayers.value = (int)Application.MaxPlayers;
        tickrate.value = Application.TickRate;
        UpdateTickrateExample(tickrate.value);

        useVoiceChat.SetEnabled(Application.IsActive);
        useVoiceChat.value = Application.UseVoiceChat && Application.IsActive;

        isServerActive.value = Application.IsActive;

        #endregion Init

        #region Callbacks

        maxPlayers.RegisterValueChangedCallback((callback) =>
        {
            panelSuccess.style.display = DisplayStyle.None;
            btnUpdate.visible = ShowUpdateButton();
        });

        tickrate.RegisterValueChangedCallback((callback) =>
        {
            panelSuccess.style.display = DisplayStyle.None;
            btnUpdate.visible = ShowUpdateButton();
            UpdateTickrateExample(tickrate.value);
        });

        useVoiceChat.RegisterValueChangedCallback((callback) =>
        {
            panelSuccess.style.display = DisplayStyle.None;
            btnUpdate.visible = ShowUpdateButton();
        });

        isServerActive.RegisterValueChangedCallback((callback) =>
        {
            panelSuccess.style.display = DisplayStyle.None;
            btnUpdate.visible = ShowUpdateButton();

            useVoiceChat.SetEnabled(isServerActive.value);
            useVoiceChat.value = Application.UseVoiceChat && isServerActive.value;
        });

        btnShowHideAppId.clicked += () =>
        {
            panelSuccess.style.display = DisplayStyle.None;
            fieldAppId.isPasswordField = !fieldAppId.isPasswordField;
        };

        btnCopyAppId.clicked += () =>
        {
            var settings = MUNSettings.Get();
            settings.AppID = Application.AppId;

            Repaint();
            ShowSuccessInfo(SUCCESSFULLY_SET_APP_ID);
        };

        btnUpdate.clicked += async () =>
        {
            loadingView.style.display = DisplayStyle.Flex;
            panelSuccess.style.display = DisplayStyle.None;
            panelErrors.style.display = DisplayStyle.None;

            var updateApplication = new UpdateApplication
            {
                appId = Application.AppId,
                isActive = isServerActive.value,
                useVoiceChat = useVoiceChat.value,
                maxPlayers = (uint)maxPlayers.value,
                tickRate = (byte)tickrate.value,
                name = Application.Name,
                regions = SelectedRegions
            };
            var isSuccess = await ServiceRequest.Application.Update(updateApplication, (err) =>
            {
                var errMsg = string.Join("; ", err.Errors.Values);
                panelErrors.Q<Label>(name: "txt-info").text = errMsg;
                panelErrors.style.display = DisplayStyle.Flex;
            });

            if (isSuccess)
            {
                Application = await ServiceRequest.Application.Get(Application.AppId);
                btnUpdate.visible = false;
                btnUpdate.SetEnabled(false);

                RepaintAppsWindow?.Invoke();
                ShowSuccessInfo(SUCCESSFULLY_UPDATED_APP);
            }

            loadingView.style.display = DisplayStyle.None;
        };

        btnDelete.clicked += () =>
        {
            panelSuccess.style.display = DisplayStyle.None;
            panelErrors.style.display = DisplayStyle.None;

            EpicVR_Accept_Acction.Init(btnDelete.text, string.Format(DELETE_APPLICATION_DESCRIPTION, Application.Name.WithBold()), Application.Name, async () =>
            {
                var isSuccess = await ServiceRequest.Application.Delete(Application.AppId, (err) =>
                {
                    var errMsg = string.Join("; ", err.Errors.Values);
                    panelErrors.Q<Label>(name: "txt-info").text = errMsg;
                    panelErrors.style.display = DisplayStyle.Flex;
                });

                if (isSuccess)
                {
                    RepaintAppsWindow?.Invoke();
                    Close();
                }
            });
        };

        #endregion Callbacks

        #region Functions

        void UpdateTickrateExample(int value)
        {
            var exampleText = tickrateExamples.Last(x => x.Key < value).Value;
            tickrateExample.SetValueWithoutNotify(exampleText);
        }

        async Task HandleRegionsAsync()
        {
            var regions = await ServiceRequest.Regions.GetAll();
            regionsList.Clear();
            regionToggls.Clear();

            for (int i = 0; i < regions.Length; i++)
            {
                var element = new Toggle("\t" + regions[i]);
                element.style.fontSize = 14;
                element.value = Application.Regions.Contains(regions[i]);

                regionsList.Add(element);
                regionToggls.Add(element);

                element.RegisterValueChangedCallback((callback) =>
                {
                    btnUpdate.visible = ShowUpdateButton();
                });
            }

            regionsList.style.minWidth = (regions.Length * 25) + 25;
        }

        bool ShowUpdateButton()
        {
            btnUpdate.SetEnabled(AnyRegionIsEnabled);

            var sameVoiceChatActivation = useVoiceChat.value.Equals(Application.UseVoiceChat);
            if (!sameVoiceChatActivation) return true;

            var sameIsServerActive = isServerActive.value.Equals(Application.IsActive);
            if (!sameIsServerActive) return true;

            var sameMaxPlayers = maxPlayers.value.Equals((int)Application.MaxPlayers);
            if (!sameMaxPlayers) return true;

            var sameTickRate = tickrate.value.Equals(Application.TickRate);
            if (!sameTickRate) return true;

            var sameRegions = SelectedRegions.SequenceEqual(Application.Regions);
            if (!sameRegions) return true;

            return false;
        }

        void ShowSuccessInfo(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                panelSuccess.style.display = DisplayStyle.None;
                return;
            }

            panelSuccess.Q<Label>(name: "txt-info").text = text;
            panelSuccess.style.display = DisplayStyle.Flex;
        }

        #endregion Functions
    }
}