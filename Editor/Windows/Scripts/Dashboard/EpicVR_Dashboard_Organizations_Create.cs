using MUN.Service;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using static MUN.EpicVR_Validators;

public class EpicVR_Dashboard_Organizations_Create : EditorWindow
{
    [SerializeField] private VisualTreeAsset createOrganizationWindow;

    public async Task CreateGUIAsync(VisualElement root)
    {
        root.Clear();
        createOrganizationWindow.CloneTree(root);
        await Task.Yield();

        #region Handle Inputs

        //Input fields:
        var fieldOrganization = root.Q<TextField>(name: "field-organization-name");

        //Validators:
        var validatorOrganizator = fieldOrganization.Q<Label>(name: "txt-validator-organization-name");

        //Buttons:
        var btnCreate = root.Q<Button>(name: "btn-create");
        btnCreate.SetEnabled(false);

        //Panels:
        var panelError = root.Q<VisualElement>(name: "panel-info-error");
        var panelSuccess = root.Q<VisualElement>(name: "panel-info-success");

        #endregion Handle Inputs

        #region Callbacks

        fieldOrganization.RegisterValueChangedCallback((callback) =>
        {
            panelSuccess.style.display = DisplayStyle.None;
            panelError.style.display = DisplayStyle.None;

            var isValid = IsValid(Type.OrganizationName, validatorOrganizator, true, fieldOrganization.value);
            btnCreate.SetEnabled(isValid);
        });

        btnCreate.clicked += async () =>
        {
            var orgName = fieldOrganization.value;
            fieldOrganization.SetValueWithoutNotify(string.Empty);
            validatorOrganizator.text = string.Empty;

            var isSuccess = await ServiceRequest.Organization.Create(orgName, (err) =>
            {
                var errMsg = string.Join("; ", err.Errors.Values);
                panelError.Q<Label>(name: "txt-info").text = errMsg;
                panelError.style.display = DisplayStyle.Flex;
            });

            if (isSuccess)
            {
                panelSuccess.style.display = DisplayStyle.Flex;
                panelError.style.display = DisplayStyle.None;
                await ServiceRequest.Organization.GetAll();
            }
        };

        #endregion Callbacks
    }
}