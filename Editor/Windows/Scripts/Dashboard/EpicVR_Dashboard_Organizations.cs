﻿using MUN.Service;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using static MUN.EpicVR_Validators;

namespace MUN
{
    public class EpicVR_Dashboard_Organizations : EditorWindow
    {
        private const string DELETE_ORGANIZATIONS_DESCRIPTION = "If you want to delete this organization, you must enter its name ({0}) in the field below. Note that the organization you are removing must not have any apps before the deletion.";
        private const string LEAVE_ORGANIZATIONS_DESCRIPTION = "If you want to leave this organization, you must enter its name ({0}) in the field below.";
        public const string NO_ORGANIZATIONS = "You do not belong to any organization. Create one. Without organization, you will not be able to create an application.";

        [SerializeField] private VisualTreeAsset listElement;

        private EpicVR_Dashboard_Organizations_Create createOrganization;
        private ServiceRequest.IOrganization selectedOrganization;

        private void OnEnable()
        {
            createOrganization = CreateInstance<EpicVR_Dashboard_Organizations_Create>();
        }

        public async Task CreateGuiAsync(VisualElement root)
        {
            if (string.IsNullOrEmpty(ServiceRequest.Account.Token)) return;

            #region Handle Fields

            //Handle Panels:
            var panelInfoErrors = root.Q<VisualElement>(name: "panel-info");
            var panelSuccess = root.Q<VisualElement>(name: "panel-info-success");
            var successTextInfo = panelSuccess.Q<Label>(name: "txt-info");

            var sectionMembers = root.Q<VisualElement>(name: "section-members");

            //Handle Dropdowns:
            var dropSelectOrganization = root.Q<DropdownField>(name: "drop-selected-organization");

            //Handle Lists:
            var memberList = root.Q<ScrollView>(name: "member-list");

            //Handle InputFields:
            var fieldAddMember = root.Q<TextField>(name: "field-add-member");

            //Handle Validators:
            var validatorAddMember = sectionMembers.Q<Label>(name: "txt-info");

            //Handle Buttons:
            var btnAddMember = fieldAddMember.Q<Button>(name: "btn-add-member");
            var btnDelete = root.Q<Button>(name: "btn-exit");
            var btnCreate = root.Q<Button>(name: "btn-create-new");

            #endregion Handle Fields

            #region Init Fields

            //Init Dropdowns:
            btnAddMember.SetEnabled(false);
            btnDelete.SetEnabled(false);

            await InitDropdownOrganizations(true);
            InitRemoveButton();

            #endregion Init Fields

            #region Callbacks

            dropSelectOrganization.RegisterValueChangedCallback(async (callback) =>
            {
                selectedOrganization = ServiceRequest.Organization.Organizations[dropSelectOrganization.index];
                btnDelete.text = selectedOrganization.IAmOwner ? "Delete organization" : "Leave organization";

                ShowSuccessText(string.Empty);

                await UpdateMembersView(selectedOrganization);
                UpdateAddMemberSection();
            });

            fieldAddMember.RegisterValueChangedCallback((callback) =>
            {
                if (panelInfoErrors.enabledSelf)
                    panelInfoErrors.style.display = DisplayStyle.None;

                var isValid = IsValid(Type.LoginOrEmail, validatorAddMember, true, fieldAddMember.value);

                if (isValid && selectedOrganization.Members.Contains(fieldAddMember.value))
                    isValid = false;

                btnAddMember.SetEnabled(isValid);
            });

            btnAddMember.clicked += async () =>
            {
                ShowSuccessText(string.Empty);
                btnAddMember.SetEnabled(false);

                var isSuccess = await ServiceRequest.Organization.AddMemberToOrganization(fieldAddMember.value, selectedOrganization.Id, (err) =>
                {
                    var errMsg = string.Join("; ", err.Errors.Values);
                    panelInfoErrors.Q<Label>(name: "txt-error-text").text = errMsg;
                    panelInfoErrors.style.display = DisplayStyle.Flex;
                });

                fieldAddMember.SetValueWithoutNotify(string.Empty);
                validatorAddMember.text = string.Empty;

                if (isSuccess)
                {
                    ShowSuccessText("User added to organization.");

                    await ServiceRequest.Organization.LoadOrganizationMembers(selectedOrganization);
                    await UpdateMembersView(selectedOrganization);
                }
            };

            btnCreate.clicked += async () =>
            {
                ShowSuccessText(string.Empty);
                await createOrganization.CreateGUIAsync(root);
            };

            #endregion Callbacks

            #region Functions

            async Task InitDropdownOrganizations(bool updateMembers)
            {
                if (ServiceRequest.Organization.Organizations == null)
                    await ServiceRequest.Organization.GetAll();

                var orgNames = GetOrganizationNames(ServiceRequest.Organization.Organizations);

                if (orgNames.Length == 0)
                {
                    sectionMembers.visible = false;
                    panelInfoErrors.Q<Label>(name: "txt-error-text").text = NO_ORGANIZATIONS;
                    panelInfoErrors.style.display = DisplayStyle.Flex;
                    return;
                }

                dropSelectOrganization.choices = new List<string>();
                foreach (var organization in orgNames)
                {
                    dropSelectOrganization.choices.Add(organization);
                }
                dropSelectOrganization.index = 0;
                selectedOrganization = ServiceRequest.Organization.Organizations[dropSelectOrganization.index];

                if (updateMembers)
                {
                    await UpdateMembersView(selectedOrganization);
                    UpdateAddMemberSection();
                }
            }

            async Task UpdateMembersView(ServiceRequest.IOrganization organization)
            {
                memberList.Clear();
                panelInfoErrors.style.display = DisplayStyle.None;

                if (organization.Members == null)
                    await ServiceRequest.Organization.LoadOrganizationMembers(organization);

                foreach (var member in organization.Members)
                {
                    var itsMyName = ServiceRequest.Account.Username.Equals(member);
                    var memberElement = listElement.Instantiate();
                    var memberName = itsMyName ? member.WithColor("#2A8163") : member;

                    memberElement.Q<Label>(name: "label-username").text = memberName;

                    var btnRemove = memberElement.Q<Button>(name: "btn-remove");
                    btnRemove.style.display = (organization.IAmOwner && !itsMyName) ? DisplayStyle.Flex : DisplayStyle.None;

                    memberList.Add(memberElement);
                    if (!btnRemove.enabledSelf) continue;

                    btnRemove.clicked += async () =>
                    {
                        bool removed = await ServiceRequest.Organization.RemoveMemberFromOrganization(member, organization.Id);
                        if (removed)
                        {
                            memberList.Remove(memberElement);
                            ShowSuccessText("User removed from organization.");
                        }
                    };
                }
            }

            void InitRemoveButton()
            {
                if (selectedOrganization is null)
                {
                    btnDelete.visible = false;
                    return;
                }

                btnDelete.text = selectedOrganization.IAmOwner ? "Delete organization" : "Leave organization";

                btnDelete.clicked += () =>
                {
                    var description = selectedOrganization.IAmOwner ? DELETE_ORGANIZATIONS_DESCRIPTION : LEAVE_ORGANIZATIONS_DESCRIPTION;
                    EpicVR_Accept_Acction.Init(btnDelete.text, string.Format(description, selectedOrganization.Name.WithBold()), selectedOrganization.Name, async () =>
                    {
                        var isSuccess = false;
                        var errMsg = string.Empty;

                        if (selectedOrganization.IAmOwner)
                        {
                            //Remove:
                            isSuccess = await ServiceRequest.Organization.Delete(selectedOrganization.Id, (err) =>
                            {
                                errMsg = string.Join("; ", err.Errors.Values);
                            });
                        }
                        else
                        {
                            //Leave:
                            isSuccess = await ServiceRequest.Organization.RemoveFromOrganization(selectedOrganization.Id, (err) =>
                            {
                                errMsg = string.Join("; ", err.Errors.Values);
                            });
                        }

                        if (isSuccess)
                        {
                            ServiceRequest.Organization.ClearOrganizations();
                            await InitDropdownOrganizations(false);
                        }
                        else if (!string.IsNullOrEmpty(errMsg))
                        {
                            panelInfoErrors.Q<Label>(name: "txt-error-text").text = errMsg;
                            panelInfoErrors.style.display = DisplayStyle.Flex;
                        }
                    });
                };

                btnDelete.SetEnabled(true);
            }

            void UpdateAddMemberSection()
            {
                fieldAddMember.style.display = selectedOrganization.IAmOwner ? DisplayStyle.Flex : DisplayStyle.None;
            }

            void ShowSuccessText(in string msg)
            {
                if (string.IsNullOrEmpty(msg))
                {
                    panelSuccess.style.display = DisplayStyle.None;
                    return;
                }

                successTextInfo.text = msg;
                panelSuccess.style.display = DisplayStyle.Flex;
            }

            #endregion Functions
        }

        private string[] GetOrganizationNames(in ServiceRequest.IOrganization[] organizations)
        {
            string[] organizationNames = new string[organizations.Length];
            for (int i = 0; i < organizationNames.Length; i++)
            {
                organizationNames[i] = organizations[i].Name;

                if (organizations[i].IAmOwner)
                    organizationNames[i] += " [owner]";
            }

            return organizationNames;
        }
    }
}