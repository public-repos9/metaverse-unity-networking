using MUN.Service;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using static MUN.EpicVR_MUN_Consts;

namespace MUN
{
    public class EpicVR_Dashboard : EditorWindow
    {
        private const string LOGGED_ASS_TEXT = "Logged as: {0}";
        private static bool isAccountTabSelected;
        private static bool isOrganizationsTabSelected;
        private static bool isApplicationsTabSelected;

        public static bool IsOpen { get; private set; }
        public static bool IsAccountTabSelected
        {
            get => isAccountTabSelected;
            set
            {
                isAccountTabSelected = value;

                if (value)
                {
                    IsOrganizationsTabSelected = false;
                    IsApplicationsTabSelected = false;
                }
            }
        }
        public static bool IsOrganizationsTabSelected
        {
            get => isOrganizationsTabSelected;
            set
            {
                isOrganizationsTabSelected = value;

                if (value)
                {
                    IsAccountTabSelected = false;
                    IsApplicationsTabSelected = false;
                }
            }
        }
        public static bool IsApplicationsTabSelected
        {
            get => isApplicationsTabSelected;
            set
            {
                isApplicationsTabSelected = value;

                if (value)
                {
                    IsOrganizationsTabSelected = false;
                    IsOrganizationsTabSelected = false;
                }
            }
        }
        private static EpicVR_Dashboard Window { get; set; }

        [SerializeField] private VisualTreeAsset dashboardWindow;

        [Space]
        [SerializeField] private VisualTreeAsset applicationsWindow;

        [SerializeField] private VisualTreeAsset organizationsWindow;
        [SerializeField] private VisualTreeAsset accountWindow;

        private EpicVR_Dashboard_Account accountDashboard;
        private EpicVR_Dashboard_Organizations organizationsDashboard;
        private EpicVR_Dashboard_Applications applicationsDashboard;

        [MenuItem("MUN/MUN Dashboard", priority = 1)]
        public static void Init()
        {
            bool isLogged = !string.IsNullOrEmpty(ServiceRequest.Account.Token);

            if (!isLogged)
            {
                EpicVR_LoginWindow.Init();
                return;
            }

            EpicVR_Dashboard wnd = GetWindow<EpicVR_Dashboard>();
            wnd.minSize = EpicVR_MUN_Consts.MIN_WIN_SIZE;
            wnd.titleContent = new GUIContent("MUN Dashboard");
        }

        private void OnEnable()
        {
            accountDashboard = CreateInstance<EpicVR_Dashboard_Account>();
            organizationsDashboard = CreateInstance<EpicVR_Dashboard_Organizations>();
            applicationsDashboard = CreateInstance<EpicVR_Dashboard_Applications>();

            IsOpen = true;
            Window = this;
        }

        private void OnDisable()
        {
            IsOpen = false;
            Window = null;
        }

        private void OnInspectorUpdate()
        {
            var loadingView = rootVisualElement.Q<VisualElement>(name: "loading-view");
            loadingView.style.display = ServiceRequest.IsSending ? DisplayStyle.Flex : DisplayStyle.None;
        }

        public async void CreateGUI()
        {
            VisualElement root = rootVisualElement;
            dashboardWindow.CloneTree(root);

            //Handle Username text:
            var loggedAsLabel = root.Q<Label>(name: "txt-logged-as");
            loggedAsLabel.text = string.Format(LOGGED_ASS_TEXT, ServiceRequest.Account.Username.WithColor(SelectedButtonColor).WithBold());

            //Handle Logout:
            var btnLogout = root.Q<Button>(name: "btn-logout");
            btnLogout.clicked += () =>
            {
                ServiceRequest.Account.Logout();
                Close();
                EpicVR_LoginWindow.Init();
            };

            //Handle Main Section:
            var mainSection = root.Q<VisualElement>(name: "section-main");

            //Handle buttons:
            var btnApps = root.Q<Button>(name: "btn-apps");
            var btnOrgs = root.Q<Button>(name: "btn-orgs");
            var btnAcct = root.Q<Button>(name: "btn-acct");
            var buttons = new Button[3] { btnApps, btnOrgs, btnAcct };

            ChangeMainSection(mainSection, applicationsWindow, buttons, 0);
            await applicationsDashboard.CreateGUIAsync(mainSection);

            btnApps.clicked += async () =>
            {
                ChangeMainSection(mainSection, applicationsWindow, buttons, 0);
                IsApplicationsTabSelected = true;
                await applicationsDashboard.CreateGUIAsync(mainSection);
            };
            btnOrgs.clicked += async () =>
            {
                ChangeMainSection(mainSection, organizationsWindow, buttons, 1);
                IsOrganizationsTabSelected = true;
                await organizationsDashboard.CreateGuiAsync(mainSection);
            };
            btnAcct.clicked += async () =>
            {
                ChangeMainSection(mainSection, accountWindow, buttons, 2);
                IsAccountTabSelected = true;
                await accountDashboard.CreateGuiAsync(mainSection);
            };
        }

        public static async void OpenTab(string tabName)
        {
            if (string.IsNullOrEmpty(tabName)) return;

            var mainSection = Window.rootVisualElement.Q<VisualElement>(name: "section-main");
            var btnApps = Window.rootVisualElement.Q<Button>(name: "btn-apps");
            var btnOrgs = Window.rootVisualElement.Q<Button>(name: "btn-orgs");
            var btnAcct = Window.rootVisualElement.Q<Button>(name: "btn-acct");
            var buttons = new Button[3] { btnApps, btnOrgs, btnAcct };

            if (tabName.Equals("account"))
            {
                Window.ChangeMainSection(mainSection, Window.accountWindow, buttons, 2);
                IsAccountTabSelected = true;
                await Window.accountDashboard.CreateGuiAsync(mainSection);

                return;
            }

            if (tabName.Equals("organizations"))
            {
                Window.ChangeMainSection(mainSection, Window.organizationsWindow, buttons, 1);
                IsOrganizationsTabSelected = true;
                await Window.organizationsDashboard.CreateGuiAsync(mainSection);

                return;
            }

            if (tabName.Equals("applications"))
            {
                Window.ChangeMainSection(mainSection, Window.applicationsWindow, buttons, 0);
                IsApplicationsTabSelected = true;
                await Window.applicationsDashboard.CreateGUIAsync(mainSection);

                return;
            }
        }

        public static void CloseWindow()
        {
            if (Window is null) return;
            Window.Close();
        }

        private void ChangeMainSection(VisualElement mainSection, VisualTreeAsset asset, Button[] buttons, int btnEnableIndex)
        {
            mainSection.Clear();

            for (int i = 0; i < buttons.Length; i++)
            {
                buttons[i].style.backgroundColor = (i == btnEnableIndex) ? SelectedButtonColor : DefaultButtonColor;
            }

            asset.CloneTree(mainSection);
        }
    }
}