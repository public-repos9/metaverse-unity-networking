# <span style="color:#53FFC4;"><br><center>Metaverse Unity Networking</center><br></span>

&emsp;Universal self-learning tool for developers
VR applications, enabling automated and easy implementation of multiplayer capabilities in VR games on various
devices. The developed solution uses advanced proprietary artificial intelligence and algorithms
machine learning.

&emsp; The developed artificial intelligence algorithms learn the specifics of a given programmer's work in real time designing a multiplayer VR game and generate dynamic hints and recommendations in the selection of the most effective methods and paths of the programmer's conduct in order to save time for implementation, and suggesting the implementation of functions most adequate for a given specificity project. The tool will allow for a significant acceleration of the programmer's work and will facilitate the organization of the source code. The use of machine learning techniques will also increase the realism of mapping the behavior of all characters participating in a multi-person VR experience through the ability to register and automatically create a library of movements based on recorded real player/user movements.

&emsp; The developed solution will be addressed to development teams and individual creators dealing with programming multiplayer gameplay in VR games and applications. Creation solutions currently available on the market Multiplayer games are not suited to creating VR experiences. Therefore, the creators use them in limited scope, and many functionalities have to be created independently from scratch. The solution proposed in the application It will therefore meet the direct needs of programmers designing multiplayer games in the VR environment.

------------

## Content:

[1. How to install](#1-how-to-install)

[2. Additional assets](#2-additional-assets)

[3. Tutorials](#3-tutorials)

[4. First steps](#4-first-steps)

[&emsp;4.1. Dashboard](#41-dashboard)

[&emsp;4.2. Asset setup](#42-asset-setup)

[&emsp;6.1 Joining the master server](#43-working-with-asset)

[5. Demo](#5-demo)

[6. First steps - scripting](#6-first-steps-scripting)

[&emsp;6.1 Joining the master server](#61-joining-the-master-server)

[&emsp;6.2 Creating the room](#62-creating-the-room)

[&emsp;6.3 Joining the room](#653-rpctargets)

[&emsp;6.4 Creating and deleting network objects](#64-creating-and-deleting-network-objects)

[&emsp;6.5 RPC sending](#65-rpc-sending)

[&emsp;6.6 Custom serialization](#66-custom-serialization)

[&emsp;6.7 Useful features](#67-useful-features)

[7. Links](#7-links)

<br>

------------


## 1. How to install:

Asset MUN (Metaverse Unity Networking) works with Unity version 2021.3 or later. To add an asset to your project, copy the link to the asset:

`https://gitlab.com/public-repos9/metaverse-unity-networking.git`

Open **PackageManager** and paste the copied link in **Add package from git URL...** (+ in the upper left corner). After a while the asset will be loaded (image below).

![load-from-git-url](readme-files/load-from-git-url.jpg)

 After successful import, you should see the imported asset in your project. Note that you are in the **Packages: In project** tab.

`Note: Your app version may be different.`

![loaded-from-git-url](readme-files/loaded-from-git-url.jpg)

You will probably see errors in the console. Clean the console. If all errors are gone, just restart Unity.

### Done! The asset is now imported correctly.
<br>
<br>

------------
## 2. Additional assets:

In additional assets you can find materials such as:
  - Demo examples using asset
  - Tutorials
  - Packages to facilitate communication with voice chat
  - Frequently used scripts and prefabs (Common Sample)
  - etc.

You can find all these add-ons in the **Package Manager** window. After selecting the **Metaverse Unity Networking** asset, just expand the **Samples** list and import the selected asset.

![samples](readme-files/samples.jpg)

The names and list of examples may differ from the one shown in the screenshot.

------------

## 3. Tutorials:

To have access to the tutorials, you must first import the tutorial package. Where will you find them? See the previous point ([Additional assets](#2-additional-assets)).

Here you will learn the most necessary information about the <b>MUN asset</b>. You will be guided step by step on how to open the appropriate windows, how to get to the main panel. You will learn how to create an organization and then an application.
You will also be presented with a window with the basic configuration of the <b>MUN asset</b>, as well as a simple application with basic scripts.
Finally, you will learn how to implement voice chat into your project.

These are the basic steps that will introduce you to an asset whose possibilities are much greater.

![tutorials](readme-files/tutorials.jpg)

------------

## 4. First steps:

The following steps are also presented in the tutorials section and will introduce you to the basic functionalities of the MUN asset. You will be presented with the MUN windows, including: creating an account; organization formation; application development; application management; management of the main MUN settings for your project. Finally, basic classes will be introduced to help you get used to the pack.

### 4.1. Dashboard:

After uploading the MUN asset, a new <b>MUN</b> tab appeared in the navigation menu as a drop-down list. The following options are available there:

- MUN Dashboard
- MUN Asset Setup
- MUN Documentation
- MUN Recommendations
- Tutorials (available after adding tutorials from Samples)

In this section, the basic functionalities of the Dashboard window will be discussed. Select <b>MUN/MUN Dashboard</b> and you should see the window below:

![samples](readme-files/dashboard-login.jpg)

If you have your own account - you can log in by entering your login and password. Otherwise - you must create an account to be able to create and manage your applications. If you have an AppId, you can immediately paste it into the <b>AppId</b> field in the <b>MUN/MUN Asset Setup</b> window.

After logging in, you will have access to three tabs:
- Apps
- Organizations
- Account

At the beginning, the list of your applications will be empty - so you need to go to the <b>Organizations</b> tab and create a new one - it shouldn't be difficult. It is important to ensure proper validation of the name of the organization and ensure its unique name.

![samples](readme-files/dashboard-orgs.jpg)

In the organization window, you can switch between multiple organizations. You can also create multiple organizations and belong to multiple other organizations. If you are an organization owner, you can also manage members of that organization. In the <i>Login/e-mail</i> field, just enter the name or email of the user to whom you want to grant access to your organization. This person will have full access to applications that have been created within this organization.

If you already have your organization - you can go back to the <b>Applications</b> tab and create your first application. In the <b>Applications</b> tab select your organization and click <b>Create Application</b> button.

![samples](readme-files/dashboard-create-app.jpg)

Several options will appear in the window, which are described in more detail below the visible window (<b>Create application FAQ</b>). Feel free to familiarize yourself with these settings.
When you have filled in the required fields, click the <b>Create</b> button.

`Please be patient, the application development process may take a few seconds. Depending on the load on the site and your connection.`

After the application has been successfully created, you will be informed by an appropriate message. Now you can switch back to the <b>Applications</b> tab (by clicking on it) and you will see the created application there.

![samples](readme-files/dashboard-apps.jpg)

Click the <b>Details</b> button to see detailed information about your application. You will also need this step to initialize the project correctly.

![samples](readme-files/dashboard-details-app.jpg)

`Please be patient, the app update process may take a few seconds. Depending on site and link load.`

In the application details, you can view the AppId (remember not to share your key, as third parties can connect to your application) and copy it to the project settings.

Click the <b>copy</b> button (copy icon) to the right of the <i>App Id</i> field.

You can also change regions (only the European region is available at the moment); the maximum number of players connected to your server at one time; change the frequency of sending packages to the server. In addition, you can completely disable the server for your application or start a server for voice chat.

In the next tab (<b>Account</b>), you can also edit your account details - nothing special, so I'll go straight to the next window: <b>MUN/MUN Asset setup</b>

### 4.2. Asset setup:

This section will cover the basic MUN asset setup window. Select <b>MUN/MUN Asset Setup</b> and you should see the following window:

![samples](readme-files/asset-setup.jpg)

If you previously copied the application ID - you should see it hidden in the <i>App Id</i> field. AppId is the unique ID of the application. With the MUN system will know which server to connect to. If you are the owner of an application that belongs to an organization in which you are a member - you can test the copied application ID, you will see a positive message if the AppId is correct.

In this window, you can also manage the logs that will be displayed in the <b>Console</b> window. If any logs annoy you, you can disable them here.

Below are links to websites that may be of additional help.

![samples](readme-files/asset-setup-rpcs.jpg)

The second tab is RPCs. Remote procedure call (RPC) - allows you to send information about calling a given function with given parameters to a group of recipients defined by you.

While developing an application, it's easy to forget what RPC names have already been used. Here you can check the names used (they should be unique) and also - if you don't see a given function here, it may turn out that something is wrong with it. Probably missing attribute [[MUNRPC]](http://vagency.smarthost.pl/mun-doc/a00223.html)

### 4.3. Working with asset:

As an addition (samples), you can import the <b>Common</b> package, which contains simple scripts and prefabs that facilitate application prototyping.

Nevertheless, this section will present the most important classes and functions that will help you orientate yourself in the asset. Detailed examples are available in the [First Steps - Scripting](#6-first-steps-scripting) section.

<br>

- <b>[MUNNetwork](http://vagency.smarthost.pl/mun-doc/a00227.html)</b> class: It has the most necessary functions that enable, among others, communication with the server:
<span style="color: #53FFC4">a)</span> Joining MasterServer
<span style="color: #53FFC4">b)</span> Create or join rooms
<span style="color: #53FFC4">c)</span> Changing scenes when connected to a room
<span style="color: #53FFC4">d)</span> Creating network objects
<span style="color: #53FFC4">e)</span> Access to your local player's details and to the room you are in

- <b>[MonobehaviourMUN](http://vagency.smarthost.pl/mun-doc/a00419.html)</b> class: Contains responses from the server for commands you execute. If you want to receive an answer, e.g. about a correct connection to the master server or about the correct creation of a room, then you should inherit from this class, and then overload the appropriate method.
- <b>[MUNView](http://vagency.smarthost.pl/mun-doc/a00427.html)</b> class: It is a representation of a network object that can serialize data between clients in the same room. The object that has this component has an owner (the person who created the object) and can send RPCs.

<br>

------------

## 5. Demo:

An example demo with the most frequently used list of functionalities can be found in the <b>Samples</b> section, about which you can read a bit more in the section on [Additional assets](#2-additional-assets).

Go to scene <b>Scene1</b> (<i>Samples/Metaverse Unity Networking/VERSION/Examples/Example_01/Scenes/Scene1</i>) and find the <b>===NetworkManager===</b> object on it. All interaction takes place in the inspector window. Press <b>Play</b>.

![samples](readme-files/demo-01.jpg)

Most options will only be available after clicking the <b>Play</b> button. At the beginning, you will be able to select any events that will be executed after given triggers. When the game is on, you can join MasterServer and create a room or join an existing one (we recommend creating a new one)

![samples](readme-files/demo-02.jpg)

`You can only change the scene once it has been added to the build settings.`

Once you're on the server, you'll see a lot more sliders, buttons, and settings that you can play around with now. Feel free to experiment with the asset. Once you know the basics, we recommend that you take a look at the code to find out how it was done. The relevant logs are displayed in the <b>Console</b> window

![samples](readme-files/demo-logs.jpg)

------------

## 6. First Steps Scripting

I assume that at this stage you already have AppId connected in MUN Asset Setup. If not, go to the [First Steps](#5-first-steps) section.

<br>

### 6.1 Joining the master server

Only one line is needed to connect to the MasterServer. It has been written in the `Start` function, which will be executed after starting the application. The `MUNExample` class inherits from the `MonoBehaviourMUN` class, which is an adapter of the `MonoBehaviour` class with several network enhancements.

```c#
using MUN.Client;

public class MUNExample : MonoBehaviourMUN
{
    private void Start()
    {
        //Connect to the master server:
        MUNNetwork.ConnectToMaster();
    }
}
```

Don't forget to put the script on the object that is in the scene.

The `ConnectToMaster` function returns a bool value, which informs us whether a packet with an attempt to join the server has been sent. It does not mean successful joining. To get this information, we need to overload the `OnConnectedToMaster` function, which is in the `MonoBehaviourMUN` class.

```c#
    public override void OnConnectedToMaster()
    {
        Debug.Log("OnConnectedToMaster Success!");
    }

    public override void OnConnectedToMasterFailed(string message)
    {
        Debug.Log($"OnConnectedToMaster Failed! Message: {message}");
    }
```

<span style=color:orange;>IMPORTANT:</span>
Most of the functions inside the `MUNNetwork` class returns a bool value as feedback on the success of sending the package to the server. This is not a substitute for information about the success of the entire operation.

<br>

### 6.2 Creating the room

One line is enough to create your own room again. It is important that the client is already connected to the MasterServer before attempting to create a room. That's why I add the line about trying to create a room in the `OnConnectedToMaster` function, not in the `Start` function.

```c#
    public override void OnConnectedToMaster()
    {
        Debug.Log("OnConnectedToMaster Success!");

        //Create room:
        MUNNetwork.CreateRoom("Fancy Room");
    }
```
When we create a room, we will automatically be connected to it. The client who creates the room becomes its owner. We can observe the progress of creating a room on two callbacks: `OnRoomCreated` and `OnJoinedRoom`.

If we want to create a room that has additional settings, we can do it using claims. To do this, create a [Room.RoomClaims](http://vagency.smarthost.pl/mun-doc/a00399.html) object and give it the appropriate parameters.

```c#
    public override void OnConnectedToMaster()
    {
        Debug.Log("OnConnectedToMaster Success!");

        Room.RoomClaims claims = Room.Claims
            .WithPassword("pass123")
            .WithMaxPlayers(10)
            .WithNewCustomProperty("game-mode", "dm");

        MUNNetwork.CreateRoom("Fancy Room", claims);
    }
```

<br>

### 6.3 Joining the room

Joining a room is as easy as creating a room. With the difference that we face certain requirements:
- We need to know the room ID (room name != room id)
- The room cannot be full
- The room cannot be closed (IsOpen = false)

You can get the Room ID from the person who is in the room:
```c# 
  var roomId = MUNNetwork.CurrentRoom.RoomId;
```
 (the ID is generated automatically after creating the room) or you can download it from the list of available (visible and not full) rooms. Let's start with the list of rooms:

```c#
    public override void OnRoomListUpdated(IEnumerable<RoomData> rooms)
    {
        foreach (var room in rooms)
        {
            Debug.Log($"RoomId: {room.RoomId}");
        }
    }
```

Just overload the function responsible for displaying the list of rooms (`OnRoomListUpdated`). This function is called automatically after joining the MasterServer or after changing the status of one of the rooms. It is also possible to call the function to refresh the list of rooms manually.

I assume you already have your dream room id. Now just one line:

```c#
    public override void OnConnectedToMaster()
    {
        Debug.Log("OnConnectedToMaster Success!");
        MUNNetwork.JoinRoom("1XYZ2");
    }
```

In case you don't want to make a list of rooms, or you want everyone to connect to the first possible room (or if there is none, create a new one), then there is also a one-line solution to your problem:

```c#
    public override void OnConnectedToMaster()
    {
        Debug.Log("OnConnectedToMaster Success!");

        MUNNetwork.JoinOrCreateRandomRoom();
    }
```
Such a room has no name, but it can accept defined claims as a function parameter. Claims will be added to the room if it is created by our client.

<br>

### 6.4 Creating and deleting network objects

Creation and deletion of network objects must be done using network functions. Otherwise (if you used the standard `MonoBehaviour.Instantiate`, the object would only be created locally.
When creating network objects, you must meet two conditions:
- This object must have a [MUNView](http://vagency.smarthost.pl/mun-doc/a00427.html) component that determines that this object can be a network object;
- This object must be in the [Resources](https://docs.unity3d.com/ScriptReference/Resources.html) folder so that (when creating an object) the MUN asset will be able to find this object.

When creating a network object, we specify the path to the file along with its name, as if we were trying to load the object from the [Resources](https://docs.unity3d.com/ScriptReference/Resources.html) folder.

```c#
    private void Update()
    {
        //Creating network objects
        if (Input.GetKeyDown(KeyCode.Space))
        {
            MUNView networkObject = MUNNetwork.CreateNetworkObject("prefab-name", Vector3.zero, Quaternion.identity);
        }

    }
```

Deleting objects is done by calling the `Destroy` function on the [MUNView](http://vagency.smarthost.pl/mun-doc/a00427.html) object. Alternatively, we can delete the network object by calling a static function in [MUNView](http://vagency.smarthost.pl/mun-doc/a00427.html) or [MUNNetwork](http://vagency.smarthost.pl/mun-doc/a00227.html).

```c#
    private void Update()
    {
        //Deleting network objects
        if (Input.GetKeyDown(KeyCode.X))
        {
            networkObject.Destroy();

            //or:
            MUNView.Destroy(networkObject);

            //or:
            MUNNetwork.DestroyNetworkObject(networkObject);
        }
    }
```
### 6.5 RPC sending

RPC or basically Remote Procedure Calls allows you to pass information to other clients in the same room so that they call the expected function. This function can also have specific parameters. Who the information about calling the function will be sent to is also strictly defined.

For example: You may want to send an RPC only to other players, or only to the room owner, or only to a specific player.

#### 6.5.1 Creating an RPC function

The RPC function must have the [[MunRPC]](http://vagency.smarthost.pl/mun-doc/a00223.html) attribute so that the MUN asset can find the function. This function cannot return anything (void)

```c#
    [MunRPC]
    private void RPC_Example()
    {
        Debug.Log("RPC_Example has been called!");
    }
```

The RPC function can also have parameters of simple type and Vector2, Vector3, Quaternion:

```c#
    [MunRPC]
    private void RPC_Example(string name, Vector3 position, Quaternion rotation)
    {
        Debug.Log("RPC_Example has been called!");
    }
```

#### 6.5.2 RPC call

We can call the RPC function using the network object. We will then be able to identify who is sending the RPC.

```c#
    private void SendRPC(int munViewId)
    {
        var munView = MUNView.Find(munViewId);
        munView.SendRPC("RPC_Example", MUNNetwork.RPCTargets.AllInRoom);

        //with parameters:
        munView.SendRPC("RPC_Example", MUNNetwork.RPCTargets.AllInRoom, "name", Vector3.zero, Quaternion.identity);
    }
```
#### 6.5.3 RPCTargets

[RPCTargets](http://vagency.smarthost.pl/mun-doc/a00227.html#a552800ca93ab08cc2d5f5bc526f7dbc4) determine to which group of recipients the RPC will be sent. The recipient can be a specific player or the audience defined below:


- <b>AllInRoom:</b> Send to all in room (sender invoke localy).

- <b>AllInRoomCache:</b> Send to all in room (sender invoke localy) save method in cache to execute for new players.

- <b>OthersInRoom:</b> Send to others in room (not invoke in sender).

- <b>OthersInRoomCache:</b> Send to others in room (not invoke in sender) save method in cache to execute for new players.

- <b>AllInRoomSynch:</b> Send to all in room.

- <b>AllInRoomSyncCache:</b> Send to all in room save method in cache to execute for new players.

- <b>RoomOwnerOnly:</b> Send to room owner only.

<br>

### 6.6 Custom serialization

Custom serialization is used when you want to send some data between all clients in the same room with a higher frequency (frequency depends on tickrate). One good example of using this functionality is character movement synchronization, where the position and rotation of the player is updated frequently.

For custom synchronization to be triggered, the client must be in the room and the object that is to send/receive packets must be a network object (has a [MUNView](http://vagency.smarthost.pl/mun-doc/a00427.html) component).

The next step is to implement the [IMUNSerialize](http://vagency.smarthost.pl/mun-doc/a00371.html) interface, which has two functions: `MUNSerializeWrite` and `MUNSerializeRead`. Both functions pass in the [MUNPackage](http://vagency.smarthost.pl/mun-doc/a00451.html) parameter a packet with synchronized data.
- The first function is used to pass (by the owner of the object) the parameters that we want to serialize.
- The second function, by analogy, is used to read serialized parameters by all other clients. Let's look at an example:

```c#
[RequireComponent(typeof(MUNView))]
public class MUNExample : MonoBehaviour, IMUNSerialize
{
    private MUNView munView;

    private void Start()
    {
        munView = GetComponent<MUNView>();
    }

    public void MUNSerializeWrite(MUNPackage packet)
    {
        packet.Write(transform.position);
        packet.Write(transform.rotation);
    }

    public void MUNSerializeRead(MUNPackage packet)
    {
        transform.position = packet.ReadVector3();
        transform.rotation = packet.ReadQuaternion();
    }
}
```
As we can see, it does not look complicated.
It is worth presenting a few additional requirements:
  - Firstly, there must be at least two clients in the room to be able to synchronize;
- Second: the order and type of read parameters must be the same as in the case of writing them;
- Third: Not all data types can be serialized. Check in [MUNPackage](http://vagency.smarthost.pl/mun-doc/a00451.html) which types can be serialized.

The fourth important issue that you may have noticed if you have already done the basic tests is the smoothness of the synchronized movement. This is because the character changes position every X time. To prevent this, an algorithm responsible for smoothing or predicting motion should be implemented. For inches of the document, we will use the simplest solution, which is lerp:

```c#
[RequireComponent(typeof(MUNView))]
public class MUNExample : MonoBehaviour, IMUNSerialize
{
    public float positionLerpSpeed = 2f;
    public float rotationLerpSpeed = 2f;

    private MUNView munView;
    private Vector3 positionFromServer;
    private Quaternion rotationFromServer;

    private void Start()
    {
        munView = GetComponent<MUNView>();
    }

    private void Update()
    {
        if (munView.IsMine) return;

        transform.position = Vector3.Lerp(transform.position, positionFromServer, Time.deltaTime * positionLerpSpeed);
        transform.rotation = Quaternion.Lerp(transform.rotation, rotationFromServer, Time.deltaTime * rotationLerpSpeed);
    }

    public void MUNSerializeWrite(MUNPackage packet)
    {
        packet.Write(transform.position);
        packet.Write(transform.rotation);
    }

    public void MUNSerializeRead(MUNPackage packet)
    {
        positionFromServer = packet.ReadVector3();
        rotationFromServer = packet.ReadQuaternion();
    }
}

```

<br>

### 6.7 Useful features</b>



- Reference to local client data. It facilitates access to the player's own properties and name. When a client joins a room, these values are synchronized with other clients in the room.
```c#
    MUNNetwork.LocalPlayer;
```

- A reference to the room where the `LocalPlayer` is currently located. In case the local player is not in the room - it returns null
```c#
    MUNNetwork.CurrentRoom;
```

- Gets the current connection status of the client to the server
```c#
    MUNNetwork.Status
```

------------

## 7. Links:

1. [Documentation](http://vagency.smarthost.pl/mun-doc/)
2. [EpicVR website](https://epicvr.pl/pl/filmy-360-gry-aplikacje-vr/)


------------
### <center>Thanks for choosing our solution <span style="color: red;">&hearts;</span></center>

<p align="center">
  <img src="readme-files/epicvr-logo.jpg" alt="epicvr.pl"/>
</p>