using MUN.Client;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace MUN.Common
{
    [CustomEditor(typeof(MUN_JoinRoom))]
    public class MUN_JoinRoom_Editor : UnityEditor.Editor
    {
        [SerializeField] VisualTreeAsset treeAsset;

        private MUN_JoinRoom joinRoom;

        private void OnEnable()
        {
            joinRoom = (MUN_JoinRoom)target;
        }

        public override VisualElement CreateInspectorGUI()
        {
            var root = treeAsset.Instantiate();
            root.Bind(new SerializedObject(joinRoom));

            var btnCreate = root.Q<Button>(name: "btn-join-room");
            var btnLeave = root.Q<Button>(name: "btn-leave-room");

            btnCreate.clicked += () =>
            {
                if (!Application.isPlaying ||
                MUNNetwork.Status != MUNNetwork.ClientStatus.ConnectedToMasterServer) return;

                joinRoom.JoinRoom();
            };

            btnLeave.clicked += () =>
            {
                if (!Application.isPlaying ||
                MUNNetwork.Status != MUNNetwork.ClientStatus.InRoom) return;

                joinRoom.LeaveRoom();
            };


            return root;
        }
    }
}