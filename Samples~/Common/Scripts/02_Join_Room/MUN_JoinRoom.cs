using MUN.Client;
using UnityEngine;

namespace MUN.Common
{
    public class MUN_JoinRoom : MonoBehaviourMUN
    {
        #region Fields
        public string roomId = string.Empty;
        public string password = string.Empty;
        #endregion

        #region Methods
        public void JoinRoom()
        {
            MUNNetwork.JoinRoom(roomId, password);
        }

        public void LeaveRoom()
        {
            MUNNetwork.LeaveRoom();
        }
        #endregion

        #region Callbacks
        public override void OnJoinedRoom()
        {
            Debug.Log("MUN_JoinRoom:".WithBold() + "OnJoinedRoom");
        }

        public override void OnJoinedRoomFailed(string message)
        {
            Debug.Log("MUN_JoinRoom:".WithBold() + $"OnJoinedRoomFailed({message})");
        }

        public override void OnLeftRoom()
        {
            Debug.Log("MUN_JoinRoom:".WithBold() + "OnLeftRoom");
        }
        #endregion
    }
}