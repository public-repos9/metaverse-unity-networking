using MUN.Client;
using UnityEngine;

namespace MUN.Common
{
    public class MUN_CreateRoom : MonoBehaviourMUN
    {
        #region Fields
        public CreateRoomType createRoomType;

        [Space]
        public string roomName = "FancyName";
        public bool isVisible = true;
        public string password = string.Empty;
        public GameMode gameMode = GameMode.Deathmatch;
        public byte maxPlayers = 4;
        #endregion

        #region Methods
        public void CreateRoom()
        {
            var claims = BuildClaims();

            switch (createRoomType)
            {
                case CreateRoomType.CreateRoom:
                    MUNNetwork.CreateRoom(roomName, claims);
                    break;
                case CreateRoomType.JoinOrCreateRoom:
                    MUNNetwork.JoinOrCreateRandomRoom(claims);
                    break;

            }
        }

        public void LeaveRoom()
        {
            MUNNetwork.LeaveRoom();
        }

        private Room.RoomClaims BuildClaims()
        {
            return Room.Claims
                  .IsVisible(isVisible)
                  .WithPassword(password)
                  .WithMaxPlayers(maxPlayers)
                  .WithNewCustomProperty("game-mode", (byte)gameMode);

        }
        #endregion

        #region Callbacks
        public override void OnRoomCreated()
        {
            Debug.Log("MUN_CreateRoom:".WithBold() + "OnRoomCreated");
        }

        public override void OnRoomCreatedFailed(string message)
        {
            Debug.Log("MUN_CreateRoom:".WithBold() + $"OnRoomCreatedFailed({message})");
        }

        public override void OnLeftRoom()
        {
            Debug.Log("MUN_CreateRoom:".WithBold() + "OnLeftRoom");
        }
        #endregion
    }
}