using MUN.Client;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace MUN.Common
{
    [CustomEditor(typeof(MUN_CreateRoom))]
    public class MUN_CreateRoom_Editor : UnityEditor.Editor
    {
        [SerializeField] VisualTreeAsset treeAsset;

        private MUN_CreateRoom createRoom;

        private void OnEnable()
        {
            createRoom = (MUN_CreateRoom)target;
        }

        public override VisualElement CreateInspectorGUI()
        {
            var root = treeAsset.Instantiate();
            root.Bind(new SerializedObject(createRoom));

            var btnCreate = root.Q<Button>(name: "btn-create-room");
            var btnLeave = root.Q<Button>(name: "btn-leave-room");

            btnCreate.clicked += () =>
            {
                if (!Application.isPlaying ||
                MUNNetwork.Status != MUNNetwork.ClientStatus.ConnectedToMasterServer) return;

                createRoom.CreateRoom();
            };

            btnLeave.clicked += () =>
            {
                if (!Application.isPlaying ||
                MUNNetwork.Status != MUNNetwork.ClientStatus.InRoom) return;

                createRoom.LeaveRoom();
            };

            return root;
        }
    }
}