﻿namespace MUN.Common
{
    public enum GameMode : byte
    {
        Deathmatch,
        TeamDeathmatch,
        Domination,
        Invasion
    }
}