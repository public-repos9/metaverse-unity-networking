﻿namespace MUN.Common
{
    public enum CreateRoomType
    {
        CreateRoom,
        JoinOrCreateRoom
    }
}