using MUN.Client;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace MUN.Common
{
    [CustomEditor(typeof(MUN_SimpleSynch))]
    public class MUN_SimpleSynch_Editor : UnityEditor.Editor
    {
        [SerializeField] VisualTreeAsset treeAsset;

        private MUN_SimpleSynch simpleSynch;

        private void OnEnable()
        {
            simpleSynch = (MUN_SimpleSynch)target;
        }

        public override VisualElement CreateInspectorGUI()
        {
            var root = treeAsset.Instantiate();
            root.Bind(new SerializedObject(simpleSynch));

            return root;
        }
    }
}