using UnityEngine;
using UnityEditor;
using UnityEngine.UIElements;
using UnityEditor.UIElements;
using MUN.Client;

namespace MUN.Common
{
    [CustomEditor(typeof(MUN_ConnectMasterServer))]
    public class MUN_ConnectMasterServer_Editor : UnityEditor.Editor
    {
        [SerializeField] VisualTreeAsset treeAsset;

        private MUN_ConnectMasterServer connectToMasterServer;
        private Label txtStatus;

        private void OnEnable()
        {
            connectToMasterServer = (MUN_ConnectMasterServer)target;
        }


        public override VisualElement CreateInspectorGUI()
        {
            var root = treeAsset.Instantiate();
            root.Bind(new SerializedObject(connectToMasterServer));

            txtStatus = root.Q<Label>(name: "txt-status");
            var btnConnect = root.Q<Button>(name: "btn-connect");
            var btnDisconnect = root.Q<Button>(name: "btn-disconnect");

            root.RegisterCallback<MouseMoveEvent>((callback) =>
            {
                txtStatus.text = string.Format("Status: {0}", MUNNetwork.Status
                                                                .ToString()
                                                                .WithBold()
                                                                .WithRandomColor());
            });

            btnConnect.clicked += () =>
            {
                if (Application.isPlaying)
                    connectToMasterServer.Connect();
            };

            btnDisconnect.clicked += () =>
            {
                if (Application.isPlaying)
                    connectToMasterServer.Disconnect();
            };

            return root;
        }

    }
}