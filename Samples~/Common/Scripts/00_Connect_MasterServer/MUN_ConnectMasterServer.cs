using MUN.Client;
using UnityEngine;

namespace MUN.Common
{
    public class MUN_ConnectMasterServer : MonoBehaviourMUN
    {
        #region Fields
        [HideInInspector]
        public bool autoConnect = true;
        #endregion

        #region Methods
        private void Start()
        {
            if (autoConnect) Connect();
        }

        public void Connect()
        {
            if (MUNNetwork.Status == MUNNetwork.ClientStatus.Disconnected)
                MUNNetwork.ConnectToMaster();
        }

        public void Disconnect()
        {
            if (MUNNetwork.Status != MUNNetwork.ClientStatus.Disconnected)
                MUNNetwork.DisconnectFromMasterServer();
        }
        #endregion

        #region Callbacks
        public override void OnConnectedToMaster()
        {
            Debug.Log("MUN_ConnectMasterServer:".WithBold() + "OnConnectedToMaster");
        }

        public override void OnConnectedToMasterFailed(string message)
        {
            Debug.Log("MUN_ConnectMasterServer:".WithBold() + $"OnConnectedToMasterFailed({message})");

        }
        #endregion
    }
}