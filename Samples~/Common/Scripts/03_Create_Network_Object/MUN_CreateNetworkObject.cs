using MUN.Client;
using System.Collections.Generic;
using UnityEngine;

namespace MUN.Common
{
    public class MUN_CreateNetworkObject : MonoBehaviour
    {
        public List<MUNView> munViews;
        public MUNView munViewPrefab;

        public MUNView CreateMUNView()
        {
            if (!Application.isPlaying || MUNNetwork.Status != MUNNetwork.ClientStatus.InRoom) return null;

            var position = (Vector3.right * Random.Range(-10f, 10f)) + (Vector3.up * Random.Range(-5f, 5f));
            return MUNNetwork.CreateNetworkObject(munViewPrefab.name, position, Quaternion.identity);
        }

        public void DestroyMUNView(MUNView munView)
        {
            if (!Application.isPlaying || MUNNetwork.Status != MUNNetwork.ClientStatus.InRoom) return;
            munView.Destroy();

            //Or:
            //MUNNetwork.DestroyNetworkObject(munView);
        }
    }
}