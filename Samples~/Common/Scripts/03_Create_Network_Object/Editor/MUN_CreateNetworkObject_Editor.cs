using MUN.Client;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace MUN.Common
{
    [CustomEditor(typeof(MUN_CreateNetworkObject))]
    public class MUN_CreateNetworkObject_Editor : UnityEditor.Editor
    {
        [SerializeField] VisualTreeAsset treeAsset;
        [SerializeField] VisualTreeAsset munViewButtonPrefab;

        private MUN_CreateNetworkObject createNetworkObject;

        private void OnEnable()
        {
            createNetworkObject = (MUN_CreateNetworkObject)target;
        }

        public override VisualElement CreateInspectorGUI()
        {
            var root = treeAsset.Instantiate();
            root.Bind(new SerializedObject(createNetworkObject));

            var munScrollView = root.Q<ScrollView>(name: "scroll-mun-view");
            var fieldPrefab = root.Q<ObjectField>(name: "field-prefab");
            var btnCreate = root.Q<Button>(name: "btn-create");

            FillScrolView();

            fieldPrefab.RegisterValueChangedCallback((callback) =>
            {
                btnCreate.SetEnabled(fieldPrefab.value != null && fieldPrefab.value is MUNView);
            });

            btnCreate.clicked += () =>
            {
                var view = createNetworkObject.CreateMUNView();
                AddToScrollView(view);
            };

            void FillScrolView()
            {
                munScrollView.Clear();
                var allViews = MUNView.FindAll(true);

                foreach (var view in allViews)
                {
                    AddToScrollView(view);
                }

            }

            void AddToScrollView(MUNView view)
            {
                if (view is null) return;

                var newView = munViewButtonPrefab.Instantiate().Q<Button>();
                var btnRemove = newView.Q<Button>(name: "btn-remove");

                newView.text = view.name;
                newView.clicked += () =>
                {
                    if (view is null) return;
                    Selection.activeGameObject = view.gameObject;
                };

                btnRemove.clicked += () =>
                {
                    if (view is null) return;
                    createNetworkObject.DestroyMUNView(view);
                    munScrollView.Remove(newView);
                };

                munScrollView.Add(newView);
            }

            return root;
        }
    }
}