using System.Linq;
using UnityEngine;

namespace MUN.AnimationRecognizer
{
    [CreateAssetMenu(fileName = "New Animations Database", menuName = "MUN/Animations/Create Animations Database")]
    internal class AnimationsData : ScriptableObject
    {
        [SerializeField]
        private AnimationData[] animationsData;

        public AnimationClip this[int index] =>
            animationsData.FirstOrDefault(x => x.Id.Equals(index)).Clip;
    }

    [System.Serializable]
    internal class AnimationData
    {
        [SerializeField] private int id;
        [SerializeField] private AnimationClip clip;

        public int Id => id;
        public AnimationClip Clip => clip;
    }
}