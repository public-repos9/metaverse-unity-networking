﻿namespace MUN.AnimationRecognizer
{
    [System.Serializable]
    internal class MLResponse
    {
        public bool success { get; set; }
        public int? movement { get; set; }
        public string? movementName { get; set; }
        public double? movementProb { get; set; }
    }
}