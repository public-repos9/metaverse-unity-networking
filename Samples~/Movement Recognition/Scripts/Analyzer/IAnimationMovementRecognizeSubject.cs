﻿namespace MUN.AnimationRecognizer
{
    internal interface IAnimationMovementRecognizeSubject
    {
        void RegisterToMoveRecognizer(IMoveRecognizeObserver observer);

        void UnregisterFromMoveRecognizer(IMoveRecognizeObserver observer);

        void RegisterToPredictNextMove(IMovePredictionObserver observer);

        void UnregisterFromPredictNextMove(IMovePredictionObserver observer);
    }
}