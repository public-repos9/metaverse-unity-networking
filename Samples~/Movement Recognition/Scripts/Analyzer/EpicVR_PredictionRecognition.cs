using UnityEngine;

namespace MUN.AnimationRecognizer
{
    [RequireComponent(typeof(EpicVR_MotionRecognition))]
    public class EpicVR_PredictionRecognition : MonoBehaviour, IMoveRecognizeObserver
    {
        private void Start()
        {
            GetComponent<EpicVR_MotionRecognition>().RegisterToMoveRecognizer(this);
        }

        private void OnDestroy()
        {
            GetComponent<EpicVR_MotionRecognition>().UnregisterFromMoveRecognizer(this);
        }

        public void OnMoveRecognized(int recognizedAnimationId)
        {
            throw new System.NotImplementedException();
        }
    }
}