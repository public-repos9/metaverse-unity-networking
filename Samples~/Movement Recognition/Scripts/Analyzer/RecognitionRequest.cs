﻿using System.Collections.Generic;

namespace MUN.AnimationRecognizer
{
    [System.Serializable]
    internal class RecognitionRequest
    {
        public RecordedSamples measurements;
        public List<int> order;
        public bool normalize;

        protected RecognitionRequest(in RecordedSamples recordedSamples, List<int> order, bool normalize)
        {
            measurements = recordedSamples;
            this.order = order;
            this.normalize = normalize;
        }

        public static RecognitionRequest Create(in RecordedSamples recordedSamples, bool normalize)
        {
            var order = new List<int>() { 1, 0, 2 };
            return new RecognitionRequest(recordedSamples, order, normalize);
        }
    }
}