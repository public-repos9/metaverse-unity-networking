﻿using System.Collections.Generic;
using System.Linq;

namespace MUN.AnimationRecognizer
{
    [System.Serializable]
    internal class PredictionRequest
    {
        public List<int> history { get; set; }

        public PredictionRequest(IEnumerable<int> history)
        {
            this.history = history.ToList();
        }
    }
}