﻿namespace MUN.AnimationRecognizer
{
    internal interface IMoveRecognizeObserver
    {
        void OnMoveRecognized(int recognizedAnimationId);
    }
}