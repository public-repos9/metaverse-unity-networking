using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using UnityEngine;

namespace MUN.AnimationRecognizer
{
    internal class EpicVR_MotionRecognition : MonoBehaviour, IAnimationMovementRecognizeSubject
    {
        private const int MAX_MOVE_HISTORY = 4;

        #region Properties

        private readonly List<IMoveRecognizeObserver> moveRecognizeObservers = new();
        private readonly List<IMovePredictionObserver> movePredictionObservers = new();

        private readonly Queue<int> moveHistory = new();

        [SerializeField]
        private string serverUrl = "http://localhost:5000";

        [SerializeField]
        private double recognitionThreshold = .8d;

        [SerializeField]
        private double predictionThreshold = .8d;

        [SerializeField]
        private bool normalize = false;

        private string RecognizeUrl { get; set; }
        private string PredictUrl { get; set; }
        private bool IsComponentActive => enabled;

        #endregion Properties

        #region Unity Methods

        private void Awake()
        {
            InitUrls();
        }

        private void OnEnable()
        {
            EpicVR_Devices_Sampler.onRecordedSamplesEvent += RecognizeMoveFromSamples;
        }

        private void OnDisable()
        {
            EpicVR_Devices_Sampler.onRecordedSamplesEvent -= RecognizeMoveFromSamples;
        }

        #endregion Unity Methods

        #region Custom Methods - Observer Pattern

        public void RegisterToMoveRecognizer(IMoveRecognizeObserver observer)
        {
            if (moveRecognizeObservers.Contains(observer))
                return;

            moveRecognizeObservers.Add(observer);
        }

        public void UnregisterFromMoveRecognizer(IMoveRecognizeObserver observer)
        {
            moveRecognizeObservers.Remove(observer);
        }

        public void RegisterToPredictNextMove(IMovePredictionObserver observer)
        {
            if (movePredictionObservers.Contains(observer)) return;
            movePredictionObservers.Add(observer);
        }

        public void UnregisterFromPredictNextMove(IMovePredictionObserver observer)
        {
            movePredictionObservers.Remove(observer);
        }

        #endregion Custom Methods - Observer Pattern

        #region Custom Methods


        private async void RecognizeMoveFromSamples(RecordedSamples recordedSamples)
        {
            if (moveRecognizeObservers.Count == 0 && movePredictionObservers.Count == 0)
                return;

            var startTime = Time.time;

            var recognitionRequest = RecognitionRequest.Create(in recordedSamples, normalize);

            var createTime = Time.time;

            string jsonString = JsonConvert.SerializeObject(recognitionRequest);

            var serializeTime = Time.time;

            string jsonResponse = await SendRequestAsync(RecognizeUrl, jsonString);

            //if (!Application.isPlaying || !IsComponentActive)
            //    return;
            var requestTime = Time.time;

            int label = ReadResponse(jsonResponse, recognitionThreshold);

            var responseTime = Time.time;

            Debug.Log($"Rozponanie: " +
                $"start={startTime}; " +
                $"end={responseTime}; " +
                $"delta = {responseTime - startTime}. " +
                $"Create: {createTime - startTime}. " +
                $"Serialize: {serializeTime - createTime}. " +
                $"Request: {requestTime - serializeTime}");

            if (label < 0)
            {
                Debug.LogError("Move not recognized");
                return;
            }

            HandleMoveHistory(label);
            Debug.Log($"Recognized move: id={label}");
            moveRecognizeObservers.ForEach(observer => observer.OnMoveRecognized(label));
        }

        private async void PredictNextMove(IEnumerable<int> movesHistory)
        {
            var content = new PredictionRequest(movesHistory);
            string jsonString = JsonConvert.SerializeObject(content);
            string jsonResponse = await SendRequestAsync(PredictUrl, jsonString);
            int prediction = ReadResponse(jsonResponse, predictionThreshold);

            if (prediction < 0)
            {
                Debug.LogError("Move not recognized");
                return;
            }

            movePredictionObservers.ForEach(observer => observer.OnMovePredicted(prediction));
        }

        private void HandleMoveHistory(int id)
        {
            moveHistory.Enqueue(id);

            if (moveHistory.Count > MAX_MOVE_HISTORY)
            {
                moveHistory.Dequeue();
                PredictNextMove(moveHistory);
            }
        }

        private void InitUrls()
        {
            RecognizeUrl = serverUrl + "/recognize";
            PredictUrl = serverUrl + "/predict";
        }

        private async Task<string> SendRequestAsync(string url, string content)
        {
            var request = HttpWebRequest.Create(url);
            request.ContentType = "application/json";
            request.Method = "POST";

            using (var sw = new StreamWriter(await request.GetRequestStreamAsync()))
            {
                await sw.WriteAsync(content);
            }

            var response = await request.GetResponseAsync();
            string jsonResponse = string.Empty;

            using (var sR = new StreamReader(response.GetResponseStream()))
            {
                jsonResponse = await sR.ReadToEndAsync();
            }

            return jsonResponse;
        }

        private int ReadResponse(string jsonString, double treshold)
        {
            var response = JsonConvert.DeserializeObject<MLResponse>(jsonString);
            var movement = response?.movement;
            var prob = response?.movementProb.GetValueOrDefault(1d);

            if (prob < treshold)
                return -1;

            return movement.GetValueOrDefault(-1);
        }

        #endregion Custom Methods
    }
}