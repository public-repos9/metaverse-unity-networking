﻿namespace MUN.AnimationRecognizer
{
    internal interface IMovePredictionObserver
    {
        void OnMovePredicted(int recognizedAnimationId);
    }
}