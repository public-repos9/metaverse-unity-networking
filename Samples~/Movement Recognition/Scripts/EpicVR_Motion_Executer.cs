using MUN.Client;
using System;
using UnityEngine;
using static MUN.Client.MUNNetwork;

namespace MUN.AnimationRecognizer
{
    [RequireComponent(typeof(MUNView))]
    internal class EpicVR_Motion_Executer : MonoBehaviour, IMoveRecognizeObserver
    {
        #region Properties

        public event Action<AnimationClip> onPlayAnimationClipEvent;

        public event Action onEndPlayAnimationClipEvent;

        private const string OVERRIDE_TRIGGER = "OverrideAnimation";
        private const string OVERRIDE_ANIMATION = "Override";

        [Header("Animator Settings")]
        [SerializeField] private Animator animator;
        [SerializeField] private AnimatorOverrideController overrideController;

        [Header("Data")]
        [SerializeField] private AnimationsData animationsData;

        [Header("Strategies")]
        [RequireInterface(typeof(IPrepareCharacterToAnimate))]
        [SerializeField] private UnityEngine.Object prepareCharacterToAnimateMethod;

        private static IAnimationMovementRecognizeSubject animationMovementRecognizeSubject;

        private bool isBusy = false;
        private MUNView munView;

        private IPrepareCharacterToAnimate PrepareCharacterToAnimate
        {
            get
            {
                if (prepareCharacterToAnimateMethod == null)
                    return null;

                return (IPrepareCharacterToAnimate)prepareCharacterToAnimateMethod;
            }
        }

        #endregion Properties

        #region Unity Methods

        private void Reset()
        {
            animator ??= GetComponentInChildren<Animator>();
            animationMovementRecognizeSubject ??= GetComponentInChildren<IAnimationMovementRecognizeSubject>();
        }

        private void Start()
        {
            InitComponents();
        }

        private void OnDestroy()
        {
            CancelInvoke();
            animationMovementRecognizeSubject?.UnregisterFromMoveRecognizer(this);
        }

        #endregion Unity Methods

        #region Custom Methods

        public void OnMoveRecognized(int recognizedAnimationId)
        {
            if (isBusy)
                return;

            var recognizedAnimation = animationsData[recognizedAnimationId];
            if (recognizedAnimation == null)
            {
                Debug.LogError($"Animation with id = {recognizedAnimationId} is null!");
                return;
            }

            if (munView.IsMine)
            {
                onPlayAnimationClipEvent?.Invoke(recognizedAnimation);
                munView.SendRPC(nameof(RPC_OnMoveRecognized), RPCTargets.OthersInRoom, recognizedAnimationId);
            }

            isBusy = true;
            Invoke(nameof(Unbusy), recognizedAnimation.length);

            if ((MUNNetwork.InRoom && !munView.IsMine) || UnityEngine.Application.isEditor)
            {
                PrepareCharacterToAnimate?.PrepareCharacter();

                overrideController[OVERRIDE_ANIMATION] = recognizedAnimation;
                animator.SetTrigger(OVERRIDE_TRIGGER);
            }
        }

        private void Unbusy()
        {
            isBusy = false;

            if (munView.IsMine)
                onEndPlayAnimationClipEvent?.Invoke();

            if ((MUNNetwork.InRoom && !munView.IsMine) || UnityEngine.Application.isEditor)
            {
                PrepareCharacterToAnimate?.UndoPreparing();
            }
        }

        private void InitComponents()
        {
            Debug.Assert(animator != null, "Animator component not found in children", gameObject);
            Debug.Assert(overrideController != null, "AnimatorOverrideController is null", gameObject);

            munView = GetComponent<MUNView>();

            if (animator != null)
                animator.runtimeAnimatorController = overrideController;

            if (!MUNNetwork.InRoom || munView.IsMine)
            {
                animationMovementRecognizeSubject ??= GameObject.FindObjectOfType<EpicVR_MotionRecognition>();
                animationMovementRecognizeSubject.RegisterToMoveRecognizer(this);
            }
        }

        #endregion Custom Methods

        #region RPCs

        [MunRPC]
        private void RPC_OnMoveRecognized(int animationId)
        {
            OnMoveRecognized(animationId);
        }

        #endregion RPCs
    }
}