using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.XR;

namespace MUN.AnimationRecognizer
{
    public class EpicVR_Devices_Sampler : MonoBehaviour, IDevicesSampler
    {
        #region Properties

        private readonly InputDeviceCharacteristics[] characteristicsOrder = new InputDeviceCharacteristics[]
        {
            InputDeviceCharacteristics.HeadMounted,
            InputDeviceCharacteristics.Left,
            InputDeviceCharacteristics.Right
        };

        internal static event Action<RecordedSamples> onRecordedSamplesEvent;

        [Tooltip("How many samples will be recorded before they are sent")]
        [SerializeField, Range(30, 300)]
        private int recordSamples = 50;

        [SerializeField, Range(5, 80)]
        private byte samplingFrequency = 80;

        [Tooltip("Automatically enables (after 5 seconds) initialization of data sampling.")]
        [SerializeField]
        private bool autoInitRecorder = true;

        private RecordedSamples recordedSamples;
        private float updateTime;
        private float currentUpdateTime;

        #endregion Properties

        #region Unity Methods

        private void Start()
        {
            if (autoInitRecorder)
                Invoke(nameof(InitRecorder), 5f);
        }

        private void Update()
        {
            HandleSampling();
        }

        #endregion Unity Methods

        #region Custom Methods

        [ContextMenu("Init Recorder")]
        public void InitRecorder()
        {
            if (recordedSamples != null)
                Debug.LogWarning("ReInit recorder");

            var devices = FindDevices();
            CalculateUpdateTime();

            recordedSamples = new RecordedSamples(in devices, samplingFrequency);
        }

        private void HandleSampling()
        {
            if (recordedSamples == null || recordedSamples.devices == null) return;

            currentUpdateTime += Time.unscaledDeltaTime;
            if (currentUpdateTime > updateTime)
            {
                currentUpdateTime -= updateTime;
                recordedSamples.RecordSample();

                HandleSendSampling();
            }
        }

        private void HandleSendSampling()
        {
            if (recordedSamples.samples.Count < recordSamples) return;
            onRecordedSamplesEvent?.Invoke(recordedSamples);
            recordedSamples.ClearSamples();
        }

        private VRDevice[] FindDevices()
        {
            Debug.Log("InitDevices: Start");

            var inputDevices = new List<InputDevice>();
            InputDevices.GetDevices(inputDevices);

            var orderedDevices = inputDevices
                .OrderBy(device => GetCharacteristicsPriority(device))
                .Select(device => new VRDevice(device))
                .ToArray();

            Debug.Log($"InitDevices: End (init: {orderedDevices.Length})");
            return orderedDevices;
        }

        private int GetCharacteristicsPriority(InputDevice device)
        {
            for (int i = 0; i < characteristicsOrder.Length; i++)
            {
                if (HasCharacteristics(device, characteristicsOrder[i]))
                    return i;
            }

            return int.MaxValue;
        }

        private bool HasCharacteristics(InputDevice device, InputDeviceCharacteristics characteristics)
    => (device.characteristics & characteristics) != 0;

        private void CalculateUpdateTime()
        {
            updateTime = 1f / samplingFrequency;
            currentUpdateTime = 0;
        }

        #endregion Custom Methods
    }
}