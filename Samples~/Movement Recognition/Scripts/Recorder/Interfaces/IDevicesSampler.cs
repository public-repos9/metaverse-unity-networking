﻿namespace MUN.AnimationRecognizer
{
    public interface IDevicesSampler
    {
        /// <summary>
        /// Initializes the recorder for character sampling.
        /// </summary>
        void InitRecorder();
    }
}