﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace MUN.AnimationRecognizer
{
    /// <summary>
    /// Represents a collection of recorded samples.
    /// </summary>
    [Serializable]
    internal sealed class RecordedSamples
    {
        [JsonProperty("TrackedDevices")]
        public VRDevice[] devices;

        public int samplingFrequency;

        [JsonProperty("Samples")]
        public List<RecordedSampleData[]> samples;

        /// <summary>
        /// Initializes a new instance of the <see cref="RecordedSamples"/> class with the specified devices and sampling frequency.
        /// </summary>
        /// <param name="devices">The array of EpicVR_Device objects representing the VR devices.</param>
        /// <param name="samplingFrequency">The sampling frequency of the recorded samples.</param>
        public RecordedSamples(in VRDevice[] devices, in int samplingFrequency)
        {
            this.devices = devices;
            this.samplingFrequency = samplingFrequency;
            samples = new List<RecordedSampleData[]>();
        }

        /// <summary>
        /// Clears all recorded samples.
        /// </summary>
        public void ClearSamples() => samples.Clear();

        /// <summary>
        /// Records a new sample for all devices.
        /// </summary>
        public void RecordSample()
        {
            var newSamples = new RecordedSampleData[devices.Length];

            for (int i = 0; i < newSamples.Length; i++)
            {
                var pos = devices[i].GetPosition();
                var rot = devices[i].GetRotation();

                newSamples[i] = new(devices[i].id, pos, rot);
            }

            samples.Add(newSamples);
        }
    }
}