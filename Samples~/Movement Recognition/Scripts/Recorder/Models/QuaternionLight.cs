﻿using System;
using UnityEngine;

namespace MUN.AnimationRecognizer
{
    /// <summary>
    /// A simplified version of Quaternion that can be wrapped in Json
    /// </summary>
    [Serializable]
    internal struct QuaternionLight
    {
        public float x, y, z, w;

        public static implicit operator QuaternionLight(Quaternion quaternion)
        {
            return new QuaternionLight
            {
                x = quaternion.x,
                y = quaternion.y,
                z = quaternion.z,
                w = quaternion.w
            };
        }
    }
}