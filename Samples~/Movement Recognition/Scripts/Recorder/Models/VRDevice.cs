﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

namespace MUN.AnimationRecognizer
{
    /// <summary>
    /// Represents a VR device that is recognized by XR package.
    /// </summary>
    [Serializable]
    internal sealed class VRDevice
    {
        private static byte DevicesCount = 0;

        public int id;
        public string[] characteristics;
        public string name;
        public string manufacturer;
        public InputDevice Device { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="VRDevice"/> class with the specified input device.
        /// </summary>
        public VRDevice(InputDevice device)
        {
            id = ++DevicesCount;
            name = device.name;
            manufacturer = device.manufacturer;
            characteristics = GetCharacteristics(device.characteristics);

            Device = device;
        }

        /// <summary>
        /// Retrieves the current position of the device.
        /// </summary>
        /// <returns>The position of the device as a light version of Vector3.</returns>
        public Vector3Light GetPosition()
        {
            if (Device.TryGetFeatureValue(CommonUsages.devicePosition, out var position))
                return position;

            return Vector3.zero;
        }

        /// <summary>
        /// Retrieves the current rotation of the device.
        /// </summary>
        /// <returns>The rotation of the device as a light version of Quaternion.</returns>
        public QuaternionLight GetRotation()
        {
            if (Device.TryGetFeatureValue(CommonUsages.deviceRotation, out var rotation))
                return rotation;

            return Quaternion.identity;
        }

        private string[] GetCharacteristics(in InputDeviceCharacteristics characteristics)
        {
            List<string> result = new List<string>();
            foreach (InputDeviceCharacteristics ch in Enum.GetValues(typeof(InputDeviceCharacteristics)))
            {
                if ((characteristics & ch) != 0) result.Add(ch.ToString());
            }

            return result.ToArray();
        }
    }
}