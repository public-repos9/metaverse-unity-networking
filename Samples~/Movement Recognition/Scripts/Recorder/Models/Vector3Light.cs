﻿using System;
using UnityEngine;

namespace MUN.AnimationRecognizer
{
    /// <summary>
    /// A simplified version of Vector3 that can be wrapped in Json
    /// </summary>
    [Serializable]
    internal struct Vector3Light
    {
        public float x, y, z;

        public static implicit operator Vector3Light(Vector3 vector)
        {
            return new Vector3Light
            {
                x = vector.x,
                y = vector.y,
                z = vector.z
            };
        }
    }
}