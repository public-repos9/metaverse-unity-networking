﻿namespace MUN.AnimationRecognizer
{
    /// <summary>
    /// Represents a serialized recorded sample data.
    /// </summary>
    [System.Serializable]
    internal struct RecordedSampleData
    {
        public int id;
        public Vector3Light position;
        public QuaternionLight rotation;

        public RecordedSampleData(int deviceId, Vector3Light position, QuaternionLight rotation)
        {
            id = deviceId;
            this.position = position;
            this.rotation = rotation;
        }
    }
}