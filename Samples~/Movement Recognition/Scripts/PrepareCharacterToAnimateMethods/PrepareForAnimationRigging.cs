using UnityEngine;
using UnityEngine.Animations.Rigging;

namespace MUN.AnimationRecognizer
{
    [RequireComponent(typeof(RigBuilder))]
    internal class PrepareForAnimationRigging : MonoBehaviour, IPrepareCharacterToAnimate
    {
        #region Properties

        private RigBuilder rigBuilder;

        #endregion Properties

        #region Unity Methods

        private void Start()
        {
            rigBuilder = GetComponent<RigBuilder>();
        }

        #endregion Unity Methods

        #region Custom Methods

        public void PrepareCharacter()
        {
            if (rigBuilder != null)
                TweenWeight(1f, 0, 1f);
        }

        public void UndoPreparing()
        {
            if (rigBuilder != null)
                TweenWeight(0, 1f, .5f);
        }

        private void TweenWeight(float fromValue, float toValue, float time)
        {
            //Install external pacckage from AssetStore: https://assetstore.unity.com/packages/tools/animation/leantween-3595
            LeanTween.value(gameObject, (value) =>
            {
                rigBuilder.layers.ForEach(layer => layer.rig.weight = value);
            }, fromValue, toValue, time);
        }

        #endregion Custom Methods
    }
}