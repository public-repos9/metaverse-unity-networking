﻿namespace MUN.AnimationRecognizer
{
    public interface IPrepareCharacterToAnimate
    {
        void PrepareCharacter();

        void UndoPreparing();
    }
}