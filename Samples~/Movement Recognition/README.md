# Motion Recognizer

## AI Server

1. There is a `.zip` package "AI Server" - extract it to the folder outside the project.

## Environment Setup

1. Download [Python 3](https://www.python.org/downloads/release/python-389/)
2. Install Python 3
3. Add Python path to environment variables
4. Launch the command line and navigate to the "motion_recognizer" directory
```
cd C:\path\to\motion_recognizer
```

5. Install a Python virtual environment
```
pip install virtualenv
```

6. Create a virtual environment
```
virtualenv venv
```

7. Activate a virtual environment
```
venv\Scripts\activate
```

8. Install the required packages

```
pip install -r requirements.txt
```

```
pip install tensorflow
```

9. Verify install *(if a tensor is returned, you've installed TensorFlow successfully)*
```
python -c "import tensorflow as tf; print(tf.reduce_sum(tf.random.normal([1000, 1000])))"
```

---

## Usage

Skip 1st and 2nd steps if you have already activated virtual environment


1. Navigate to the "motion_recognizer" directory

```
cd C:\path\to\motion_recognizer
```
2. Activate a virtual environment

```
venv\Scripts\activate
```
3. Run the `server.py` script
```
python server.py
```
*(if you have problems with port - check [Troubleshooting](##Troubleshooting) section)*

4. Once the script is complete, deactivate the virtual environment
```
deactivate
```

---

## Troubleshooting

### Issue: Port 5000 is Busy When Running `server.py`

**Symptom:** When running the `server.py` script, you encounter an error message indicating that port 5000 is busy. The error message may resemble the following:

```
An attempt was made to access a socket in a way forbidden by its access permissions.
```

**Solution:**

1. Open the `server.py` script in a text editor or code editor of your choice.
2. Locate the line where the Flask application is run. This line typically looks like:

```
app.run()
```

3. Change the port number from 5000 to an available port number, for example, 5001. Modify the line to look like this:

```
app.run(port=5001)
```
*Ensure that the new port number is not already in use by another application.*

4. Save the changes to the `server.py` script.
5. If you made changes to the port number in `server.py`, make sure to update the ServerUrl field of the *EpicVR_MotionRecognition* component of the *Motion Recognition Manager* object to reflect the new port number (5001 in this example).

Now, when you run the server.py script, it should use port 5001 instead of 5000, and you should not encounter the "port busy" issue.

---