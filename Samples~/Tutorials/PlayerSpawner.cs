using MUN.Client;
using UnityEngine;

namespace MUN.Tutorial
{
    public class PlayerSpawner : MonoBehaviourMUN
    {
        [SerializeField] private GameObject playerPrefab;

        public override void OnJoinedRoom()
        {
            MUNNetwork.CreateNetworkObject(playerPrefab.name, transform.position, Quaternion.identity);
        }
    }
}
