using MUN.Client;
using UnityEngine;

namespace MUN.Tutorial
{
    public class VoiceChatManager : MonoBehaviourMUN
    {
        [SerializeField] private GameObject voiceChatConnector;
        
        public override void OnConnectedToMaster()
        {
            Instantiate(voiceChatConnector);
        }
    }
}
