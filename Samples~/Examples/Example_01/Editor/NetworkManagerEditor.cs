using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using MUN.Client;

[CustomEditor(typeof(NetworkManager))]
public class NetworkManagerEditor : Editor
{
    SerializedProperty playerName;
    SerializedProperty executeTestOnPlayerJoined;
    SerializedProperty executeTestOnJoined;
    SerializedProperty munViewId;
    SerializedProperty disconnectFromRoomOnRoomOwnerChanged;
    SerializedProperty executeTestOnSceneChangedRoomOwner;
    SerializedProperty playerId;

    private NetworkManager networkManager;

    private void OnEnable()
    {
        networkManager = (NetworkManager)target;
        playerName = serializedObject.FindProperty("playerName");
        executeTestOnPlayerJoined = serializedObject.FindProperty("executeTestOnPlayerJoined");
        executeTestOnJoined = serializedObject.FindProperty("executeTestOnJoined");
        munViewId = serializedObject.FindProperty("munViewId");
        disconnectFromRoomOnRoomOwnerChanged = serializedObject.FindProperty("disconnectFromRoomOnRoomOwnerChanged");
        executeTestOnSceneChangedRoomOwner = serializedObject.FindProperty("executeTestOnSceneChangedRoomOwner");
        playerId = serializedObject.FindProperty("playerId");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(playerName, new GUIContent("Nazwa gracza:"));
        EditorGUILayout.PropertyField(executeTestOnPlayerJoined, new GUIContent("Wykonywane testy przez RoomOwner:"));
        EditorGUILayout.PropertyField(executeTestOnJoined, new GUIContent("Wykonywane testy po do��czeniu:"));
        EditorGUILayout.PropertyField(executeTestOnSceneChangedRoomOwner, new GUIContent("Wykonaj po zmianie sceny przez RoomOwner:"));

        GUILayout.Space(10);

        EditorGUILayout.PropertyField(disconnectFromRoomOnRoomOwnerChanged, new GUIContent("Wyjd� po zmianie RoomOwnera:"));

        GUILayout.Space(10);

        if (!Application.isPlaying)
        {
            EditorGUILayout.HelpBox("Edytor dost�pny po uruchomieniu aplikacji.", MessageType.Info);
            serializedObject.ApplyModifiedProperties();
            return;
        }


        GUILayout.Label($"Status gracza: {MUNNetwork.Status}", GUI.skin.box);

        if (MUNNetwork.Status == MUNNetwork.ClientStatus.Disconnected)
        {
            if (GUILayout.Button("Po��cz")) networkManager.ConnectToMaster();
        }
        else if (MUNNetwork.Status == MUNNetwork.ClientStatus.ConnectedToMasterServer)
        {
            if (GUILayout.Button("Do��cz")) networkManager.JoinOrCreateRandomRoom();
            if (GUILayout.Button("Roz��cz")) networkManager.DisconnectFromMaster();
        }
        else if (MUNNetwork.Status == MUNNetwork.ClientStatus.InRoom)
        {
            GUILayout.Label($"Liczba graczy: {MUNNetwork.CurrentRoom.PlayersInRoom}", GUI.skin.box);
            Player playerToManage = null;

            EditorGUILayout.PropertyField(munViewId, new GUIContent("MUNView do zarz�dzania:"));

            if (munViewId.intValue <= 0)
            {
                EditorGUILayout.HelpBox("Podaj warto�� MUNView Id > 0", MessageType.Info);
            }
            else if (MUNView.Find(munViewId.intValue) is null)
            {
                EditorGUILayout.HelpBox("Nieznaleziono MUNView o podanym Id.", MessageType.Error, true);
            }

            EditorGUILayout.PropertyField(playerId, new GUIContent("Player Id do zarz�dzania:"));

            if (playerId.intValue < 0)
            {
                EditorGUILayout.HelpBox("Podaj warto�� Player Id >= 0", MessageType.Info);
            }
            else
            {
                playerToManage = MUNNetwork.CurrentRoom.GetPlayer(playerId.intValue);

                if (playerToManage is null)
                    EditorGUILayout.HelpBox("Nieznaleziono gracza o podanym Id.", MessageType.Error, true);
            }

            GUILayout.BeginVertical();
            foreach (var player in MUNNetwork.CurrentRoom.Players)
            {
                GUILayout.BeginHorizontal();

                GUI.color = player.Value.IsRoomOwner ? Color.green : Color.white;
                GUILayout.Label($"({player.Key}) {player.Value.Name}");
                GUI.color = Color.white;

                if (MUNNetwork.LocalPlayer.IsRoomOwner)
                {
                    if (GUILayout.Button("Kick")) player.Value.KickPlayer();
                    if (GUILayout.Button("Room Owner")) MUNNetwork.CurrentRoom.ChangeOwner(player.Value);
                }

                GUILayout.EndHorizontal();

                GUILayout.BeginVertical(GUI.skin.box);
                GUILayout.Label("Properties:");
                foreach (var property in player.Value.Properties)
                {
                    GUILayout.Label($"{property.Key.ToString()} - {property.Value.ToString()}");
                }
                GUILayout.EndVertical();
            }
            GUILayout.EndVertical();

            GUILayout.Space(10);

            GUILayout.BeginVertical();
            GUILayout.Label("W�a�ciwo�ci pokoju:", GUI.skin.box);
            foreach (var property in MUNNetwork.CurrentRoom.Properties)
            {
                GUILayout.Label($"{property.Key.ToString()} - {property.Value.ToString()}");
            }
            GUILayout.EndVertical();

            GUILayout.Space(10);

            GUILayout.BeginVertical();
            GUILayout.Label("MUN Views:", GUI.skin.box);
            foreach (var munView in FindObjectsOfType<MUNView>())
            {
                GUILayout.BeginHorizontal();

                GUILayout.Label($"({munView.Id}) {munView.name}");

                if (munView.Owner.IsLocal || munView.IsMine)
                {
                    if (GUILayout.Button("Destroy")) munView.Destroy();

                    GUI.enabled = playerToManage != null;
                    if (GUILayout.Button("Zmie� w�a�ciciela")) munView.ChangeOwner(playerToManage);
                    GUI.enabled = true;
                }

                GUILayout.EndHorizontal();
            }
            GUILayout.EndVertical();

            if (GUILayout.Button("Zamie� scen�")) networkManager.SwitchScene();
            if (GUILayout.Button("Utw�rz MUNView")) networkManager.CreateNewMUNView();
            if (GUILayout.Button("Opu�� pok�j")) networkManager.LeftRoom();
            if (GUILayout.Button("Roz��cz")) networkManager.DisconnectFromMaster();
        }


        serializedObject.ApplyModifiedProperties();
    }
}
