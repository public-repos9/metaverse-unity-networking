using MUN.Client;

public class KickNewPlayer : IExecuteTest
{
    public void Execute(params object[] args)
    {
        if (args is null || args.Length == 0)
        {
            throw new System.ArgumentException($"Klasa {this.GetType().Name} wymaga argumentu nowego gracza.");
        }

        if (args[0] is Player)
        {
            KickPlayer((Player)args[0]);
        }
        else
        {
            throw new System.InvalidCastException($"Klasa {GetType().Name} wymaga argumentu Player.");
        }
    }

    private void KickPlayer(Player newPlayer)
    {
        newPlayer.KickPlayer();
    }
}
