using MUN.Client;
using System;

public class ChangeRoomOwner : IExecuteTest
{
    public void Execute(params object[] args)
    {
        if (args is null || args.Length == 0)
        {
            throw new ArgumentException($"Klasa {this.GetType().Name} wymaga argumentu nowego gracza.");
        }

        if (args[0] is Player)
        {
            ChangeRoomOwnerForNewPlayer((Player)args[0]);
        }
        else
        {
            throw new InvalidCastException($"Klasa {GetType().Name} wymaga argumentu Player.");
        }
    }

    private void ChangeRoomOwnerForNewPlayer(Player player)
    {
        if (!MUNNetwork.LocalPlayer.IsRoomOwner) return;
        MUNNetwork.CurrentRoom.ChangeOwner(player);
    }
}
