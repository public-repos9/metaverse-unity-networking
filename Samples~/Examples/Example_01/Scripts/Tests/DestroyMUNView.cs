﻿using MUN.Client;
using System;
public class DestroyMUNView : IExecuteTest
{
    public void Execute(params object[] args)
    {
        if (args is null || args.Length == 0)
        {
            throw new ArgumentException($"Klasa {this.GetType().Name} wymaga argumentu nowego gracza.");
        }

        if (args[1] is int)
        {
            DestronyNetworkObject((int)args[1]);
        }
        else
        {
            throw new InvalidCastException($"Klasa {GetType().Name} wymaga argumentu Player.");
        }
    }

    private void DestronyNetworkObject(int munViewId)
    {
        MUNView munView = MUNView.Find(munViewId);

        if (munView != null) //throw new NullReferenceException($"Nieznaleziono MUNView dla Id = {munViewId}");
            munView.Destroy();
    }
}
