using System;

[Flags]
public enum ExecuteTestType
{
    KickNewPlayer = 1,
    ChangeRoomOwnerToNewPlayer = 2,
    ChangeSceneOnNewPlayerJoin = 4,
    DisconnectFromRoom = 8,
    AddRoomProperty = 16,
    AddPlayerProperties = 32,
    ChangeMUNViewOwner = 64,
    DestroyMUNView = 128,
    SendRPC = 256,
}
