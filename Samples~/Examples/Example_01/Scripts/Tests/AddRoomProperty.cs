﻿using MUN.Client;
using System;

public class AddRoomProperty : IExecuteTest
{
    public void Execute(params object[] args)
    {
        if (args is null || args.Length == 0)
        {
            throw new ArgumentException($"Klasa {this.GetType().Name} wymaga argumentów.");
        }

        LightHashTable properties = new LightHashTable();
        const string PROPERTY = "property_{0}";

        for (int i = 0; i < args.Length; i++)
        {
            properties.Add(string.Format(PROPERTY, i), args[i].ToString());
        }

        MUNNetwork.CurrentRoom.SetCustomProperties(properties);
    }
}
