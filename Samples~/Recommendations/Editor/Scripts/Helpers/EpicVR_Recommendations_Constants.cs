using CodeAnalysis;

namespace MUN.Recommendations
{
    public static class EpicVR_Recommendations_Constants
    {
        public const string SCRIPTS_TO_ANALISE = "ScriptsToAnalise.txt";
        public const string LAST_MODIFIED_SCRIPTS = "LastModified.txt";

        public static string GetStartPragma(CodeError type)
        {
            return $"#pragma warning disable MunRecommendationIgnore:{(int)type}";
        }

        public static string GetEndPragma(CodeError type)
        {
            return $"#pragma warning restore MunRecommendationIgnore:{(int)type}";
        }
    }
}