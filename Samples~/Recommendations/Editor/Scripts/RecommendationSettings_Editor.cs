using UnityEditor;
using UnityEngine;

namespace MUN.Recommendations
{
    [CustomEditor(typeof(RecommendationSettings))]
    public class RecommendationSettings_Editor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Show"))
            {
                EpicVR_RecommendationsWindow.Init();
            }
        }
    }
}