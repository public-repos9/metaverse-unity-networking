﻿namespace MUN.Recommendations
{
    public interface IRecommendationFilters
    {
        bool FilterControlFlowManagement { get; }
        bool FilterIncorrectAssigment { get; }
        bool FilterInitialization { get; }
        bool FilterLogical { get; }
        bool FilterMissingThrow { get; }
        bool FilterVR { get; }
    }
}