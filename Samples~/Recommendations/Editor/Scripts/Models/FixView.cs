using CodeAnalysis;

namespace MUN.Recommendations
{
    public class FixView
    {
        /// <summary>
        /// Fix content.
        /// </summary>
        public string Text { get; }

        /// <summary>
        /// Fix description.
        /// </summary>
        public string Description { get; }

        /// <summary>
        /// Index of the fix beginning.
        /// </summary>
        public int Start { get; private set; }

        /// <summary>
        /// Index of the fix end. It excludes itself.
        /// </summary>
        public int Stop { get; private set; }

        public CodeError Type { get; }

        public bool Ignored { get; set; }
        public bool Applied { get; set; }

        public FixView(Fix fix)
        {
            Text = fix.Text;
            Description = fix.Message;
            Start = fix.Start;
            Stop = fix.Stop;
            Type = fix.ErrorType;
        }

        /// <summary>
        /// Move "Start" and "Stop" indexes by "i" value.
        /// </summary>
        /// <param name="i"></param>
        public void MoveIndexes(int i)
        {
            Start += i;
            Stop += i;
        }
    }
}