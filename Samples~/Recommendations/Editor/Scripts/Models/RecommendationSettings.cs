﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace MUN.Recommendations
{
    public class RecommendationSettings : ScriptableObject, IRecommendationFilters
    {
        #region Variables

        [SerializeReference, HideInInspector]
        private bool openOnScriptsCompile = true;

        [SerializeReference, HideInInspector]
        private bool isEnabled = true;

        [SerializeReference, HideInInspector]
        private bool filterLogical = true;

        [SerializeReference, HideInInspector]
        private bool filterMissingThrow = true;

        [SerializeReference, HideInInspector]
        private bool filterControlFlowManagement = true;

        [SerializeReference, HideInInspector]
        private bool filterIncorrectAssigment = true;

        [SerializeReference, HideInInspector]
        private bool filterInitialization = true;

        [SerializeReference, HideInInspector]
        private bool filterVR = true;

        private static RecommendationSettings recommendationSettings;

        #endregion Variables

        #region Properties

        public bool OpenOnScriptsCompile => openOnScriptsCompile;
        public bool IsEnabled => isEnabled;
        public bool FilterLogical => filterLogical;
        public bool FilterMissingThrow => filterMissingThrow;
        public bool FilterControlFlowManagement => filterControlFlowManagement;
        public bool FilterIncorrectAssigment => filterIncorrectAssigment;
        public bool FilterInitialization => filterInitialization;
        public bool FilterVR => filterVR;

        private static bool HasRecommendationsFiltersFile => File.Exists("Assets/EpicVR/Resources/Data/RecommendationSettings.asset");

        #endregion Properties

        public static RecommendationSettings Get()
        {
            if (recommendationSettings is null)
            {
                if (!HasRecommendationsFiltersFile)
                    recommendationSettings = CreateRecommendationSettings();
                else
                    recommendationSettings = Resources.Load<RecommendationSettings>("Data/RecommendationSettings");
            }

            return recommendationSettings;
        }

        private static RecommendationSettings CreateRecommendationSettings()
        {
            var folder = Path.GetFullPath("Assets/EpicVR/Resources/Data");

            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            var settings = CreateInstance<RecommendationSettings>();

            AssetDatabase.CreateAsset(settings, "Assets/EpicVR/Resources/Data/RecommendationSettings.asset");
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            return settings;
        }
    }
}