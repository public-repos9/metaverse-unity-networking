using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;

namespace MUN.Recommendations
{
    public class EpicVR_Recommendations_DetectScriptsChanges : AssetPostprocessor
    {
        private const string IGNORE_WORD = "MUN";
        private const string CS_EXTENSION = ".cs";

        [DidReloadScripts]
        private static void OnScriptsReloaded()
        {
            var paths = FilePaths.GetPaths(EpicVR_Recommendations_Constants.LAST_MODIFIED_SCRIPTS);
            var settings = RecommendationSettings.Get();

            if (paths.Length > 0 && settings.IsEnabled)
                EpicVR_RecommendationsWindow.OnScriptsChanged(paths, settings.OpenOnScriptsCompile);
        }

        private static void OnPostprocessAllAssets(
            string[] importedAssets,
            string[] deletedAssets,
            string[] movedAssets,
            string[] movedFromAssetPaths)
        {
            foreach (string path in importedAssets)
            {
                if (path.Contains(IGNORE_WORD)) continue;

                if (PathHasExtension(in path, CS_EXTENSION) && !PathIsFromPackagesFolder(in path))
                    FilePaths.AddPath(EpicVR_Recommendations_Constants.LAST_MODIFIED_SCRIPTS, in path);
            }
        }

        private static bool PathIsFromPackagesFolder(in string path)
            => path.StartsWith("packages", System.StringComparison.OrdinalIgnoreCase);

        private static bool PathHasExtension(in string path, in string expectedExtension)
        {
            if (!Path.HasExtension(path))
                return false;

            var pathExtension = Path.GetExtension(path);
            return pathExtension == expectedExtension;
        }
    }
}