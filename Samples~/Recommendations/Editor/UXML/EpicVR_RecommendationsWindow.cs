using CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace MUN.Recommendations
{
    public class EpicVR_RecommendationsWindow : EditorWindow
    {
        [SerializeField] private VisualTreeAsset treeAsset;
        [SerializeField] private VisualTreeAsset recommendationPrefab;

        private static bool isEnabled;
        private static CodeAnalyzer codeAnalyzer;
        private static List<Recommendation> recommendations = new List<Recommendation>();

        private static CodeAnalyzer CodeAnalyzer
        {
            get
            {
                if (codeAnalyzer is null)
                {
                    string projectPath = Path.GetFullPath("Packages/com.epicvr.mun/Editor/Plugins/CodeAnalysis");
                    string modelPath = Path.Combine(projectPath, "model.zip");
                    codeAnalyzer = new CodeAnalyzer(projectPath, modelPath);
                }

                return codeAnalyzer;
            }
        }

        [MenuItem("MUN/MUN Recommendations")]
        public static void Init()
        {
            EpicVR_RecommendationsWindow wnd = GetWindow<EpicVR_RecommendationsWindow>();
            wnd.minSize = new Vector2(720, 512);
            wnd.titleContent = new GUIContent("MUN Recommendations");

            if (!isEnabled)
                OnScriptsChanged(Array.Empty<string>(), false);
            wnd.Show();
        }

        private void OnEnable()
        {
            isEnabled = true;
        }

        private void OnDisable()
        {
            isEnabled = false;
        }

        public void CreateGUI()
        {
            VisualElement root = rootVisualElement;
            treeAsset.CloneTree(root);

            var settings = RecommendationSettings.Get();
            var recommendationSettings = new SerializedObject(settings);

            #region Handle Fields

            var errorList = root.Q<ScrollView>(name: "errors-list");
            var filters = root.Q<Foldout>(name: "foldout-filters").Query<Toggle>().ToList();

            var txtRecommendationsCount = root.Q<Label>(name: "txt-recommendations-count");
            var btnApplyAll = root.Q<Button>(name: "btn-apply-all");
            var btnIgnoreAll = root.Q<Button>(name: "btn-ignore-all");

            #endregion Handle Fields

            #region Init

            root.Bind(recommendationSettings);

            DrawRecommendationsCount();
            DrawRecommendations();

            #endregion Init

            #region Callbacks

            foreach (var filter in filters)
            {
                filter.RegisterValueChangedCallback((callback) =>
                {
                    DrawRecommendationsCount();
                    DrawRecommendations();
                });
            }

            btnApplyAll.clicked += () =>
            {
                foreach (var recommendation in recommendations)
                {
                    ExecuteAllButton(recommendation, recommendation.ProvideFix);
                }

                AssetDatabase.Refresh();
            };

            btnIgnoreAll.clicked += () =>
            {
                foreach (var recommendation in recommendations)
                {
                    ExecuteAllButton(recommendation, recommendation.IgnoreFix);
                }

                AssetDatabase.Refresh();
            };

            #endregion Callbacks

            #region Functions

            void ExecuteAllButton(Recommendation recommendation, Action<FixView, bool> action)
            {
                foreach (var fix in recommendation.Fixes)
                {
                    if (ShouldFilterFix(fix, settings))
                        action?.Invoke(fix, false);
                }
            }

            void DrawRecommendationsCount()
            {
                var visableRecommendationsCount = recommendations.Sum(x => x.Fixes.Count(f => ShouldFilterFix(in f, settings) && !f.Ignored && !f.Applied));
                var recommendationsCount = recommendations.Sum(x => x.Fixes.Count(f => !f.Ignored && !f.Applied));
                txtRecommendationsCount.text = $"{visableRecommendationsCount}/{recommendationsCount}";
            }

            void DrawRecommendations()
            {
                errorList.Clear();

                foreach (var recommendation in recommendations)
                {
                    foreach (var fix in recommendation.Fixes)
                    {
                        if (!ShouldFilterFix(in fix, settings) || fix.Ignored || fix.Applied)
                        {
                            continue;
                        }
                        DrawRecommendation(recommendation, fix);
                    }
                }
            }

            void DrawRecommendation(Recommendation recommendation, FixView fix)
            {
                var foldoutFixRecommendation = recommendationPrefab.Instantiate().Q<Foldout>();
                var foldoutBefore = foldoutFixRecommendation.Q<Foldout>(name: "foldout-before");
                var foldoutAfter = foldoutFixRecommendation.Q<Foldout>(name: "foldout-after");
                var txtCodeBefore = foldoutBefore.Q<Label>(name: "txt-code");
                var txtCodeAfter = foldoutAfter.Q<Label>(name: "txt-code");
                var btnApply = foldoutFixRecommendation.Q<Button>(name: "btn-apply");
                var btnIgnore = foldoutFixRecommendation.Q<Button>(name: "btn-ignore");

                foldoutFixRecommendation.text = $"{recommendation.FileName.ToUpper().WithBold()} [{fix.Type}] - {fix.Description.WithItalic()}";

                var currentCodeLines = recommendation
                    .AddBoldTags(fix)
                    .Split(Environment.NewLine)
                    .Skip(recommendation.GetStartLineIndex(fix))
                    .Take(recommendation.GetLinesCount(fix))
                    .ToArray();

                AddTagsAfterSplitToLines(ref currentCodeLines);

                var afterCodeLines = recommendation.Fix(fix, true)
                    .Split(Environment.NewLine)
                    .Skip(recommendation.GetStartLineIndex(fix))
                    .Take(recommendation.GetFixLinesCount(fix))
                    .ToArray();

                AddTagsAfterSplitToLines(ref afterCodeLines);

                DrawCodeSection(recommendation, fix, txtCodeBefore, currentCodeLines, true);
                DrawCodeSection(recommendation, fix, txtCodeAfter, afterCodeLines, false);

                btnApply.clicked += () =>
                {
                    recommendation.ProvideFix(fix, true);
                };

                btnIgnore.clicked += () =>
                {
                    recommendation.IgnoreFix(fix, true);
                };

                errorList.Add(foldoutFixRecommendation);
            }

            void DrawCodeSection(Recommendation recommendation, FixView fix, Label section, string[] codeLines, bool isCurrentCode)
            {
                var sb = new StringBuilder();

                if (!isCurrentCode && fix.Text.Length == 0)
                {
                    sb.AppendLine("\t<b>The code will be removed.</b>");
                    section.text = sb.ToString();
                    return;
                }

                var lineNumber = recommendation.GetStartLineIndex(fix) + 1;
                foreach (var line in codeLines)
                {
                    sb.Append(lineNumber++);
                    sb.Append(" | ");
                    sb.Append(line);
                    sb.AppendLine();
                }

                section.text = sb.ToString();
            }

            #endregion Functions
        }

        public static void OnScriptsChanged(IList<string> changedScripts, bool runInit = true)
        {
            if (changedScripts.Count == 0) return;
            var pathsToAnalise = SavePathsToAnalise(in changedScripts);
            recommendations.Clear();

            foreach (var path in pathsToAnalise)
            {
                if (!File.Exists(path))
                {
                    FilePaths.RemovePath(EpicVR_Recommendations_Constants.SCRIPTS_TO_ANALISE, in path);
                    continue;
                }

                var code = ReadFileCRLF(path);
                var fixRaw = CodeAnalyzer.Repair(code);
                var fixes = fixRaw.Where(x => x is not null)
                    .Select(x => new FixView(x))
                    .ToArray();

                if (fixes?.Length == 0)
                {
                    FilePaths.RemovePath(EpicVR_Recommendations_Constants.SCRIPTS_TO_ANALISE, in path);
                    continue;
                }

                var recommendation = new Recommendation(path, code, fixes);
                recommendations.Add(recommendation);

                if (recommendations.Count > 0 && runInit) Init();
            }
        }

        private static string ReadFileCRLF(string path)
        {
            var result = new StringBuilder();

            using (var reader = new StreamReader(path))
            {
                string line = string.Empty;
                while ((line = reader.ReadLine()) != null)
                {
                    result.AppendLine(line);
                }
            }

            return result.ToString();
        }

        private bool ShouldFilterFix(in FixView fix, in IRecommendationFilters filters)
        {
            switch (fix.Type)
            {
                case CodeError.LogicalError:
                    return filters.FilterLogical;

                case CodeError.MissingThrowError:
                    return filters.FilterMissingThrow;

                case CodeError.ControlFlowManagementError:
                    return filters.FilterControlFlowManagement;

                case CodeError.IncorrectAssignmentError:
                    return filters.FilterIncorrectAssigment;

                case CodeError.InitializationError:
                    return filters.FilterInitialization;

                case CodeError.VRError:
                    return filters.FilterVR;

                default:
                    Debug.Log("Unknown CodeError type");
                    return false;
            }
        }

        /// <summary>
        /// When code is splitting into lines that process can break highlight tags.
        /// Method repairs tags.
        /// </summary>
        /// <param name="codeLines"></param>
        private static void AddTagsAfterSplitToLines(ref string[] codeLines)
        {
            var collectLinesToBold = false;
            var lines = new List<int>();
            for (int i = 0; i < codeLines.Length; i++)
            {
                if (collectLinesToBold)
                {
                    lines.Add(i);
                }

                if (codeLines[i].Contains("<b>")
                   && !codeLines[i].Contains("</b>"))
                {
                    codeLines[i] = $"{codeLines[i]}</b>";
                    collectLinesToBold = true;
                }
                else if (codeLines[i].Contains("</b>")
                         && !codeLines[i].Contains("<b>"))
                {
                    codeLines[i] = $"<b>{codeLines[i]}";
                    collectLinesToBold = false;
                    lines.Remove(i);
                }
            }

            foreach (int lineIndex in lines)
                codeLines[lineIndex] = $"<b>{codeLines[lineIndex]}</b>";
        }

        private static string[] SavePathsToAnalise(in IList<string> changedScripts)
        {
            var changedScriptsWithoutEmpty = changedScripts.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            var savedPaths = FilePaths.GetPaths(EpicVR_Recommendations_Constants.SCRIPTS_TO_ANALISE);

            changedScriptsWithoutEmpty = changedScripts.Select(GetFullPath).ToArray();
            var resultPaths = new List<string>(savedPaths);

            foreach (var script in changedScripts)
            {
                if (savedPaths.Contains(script)) continue;

                resultPaths.Add(script);
                FilePaths.AddPath(EpicVR_Recommendations_Constants.SCRIPTS_TO_ANALISE, in script);
            }

            return resultPaths.ToArray();
        }

        private static string GetFullPath(string path)
        {
            var appPathMembers = Application.dataPath.Split('/', '\\');
            appPathMembers = appPathMembers.Take(appPathMembers.Length - 1).ToArray();
            var pathMembers = path.Split('/', '\\');

            return Path.Combine(string.Join("/", appPathMembers), string.Join("/", pathMembers));
        }
    }
}