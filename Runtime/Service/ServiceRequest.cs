﻿/// <summary>
/// Main namespace
/// </summary>
namespace MUN.Service
{
    /// <summary>
    /// The class responsible for connecting to the service and the exchange of information between service and the client
    /// </summary>
    public static partial class ServiceRequest
    {
        /// <summary>
        /// Metaverse Service URL
        /// </summary>
        private const string URL = @"https://mun.epicvr.pl";

        /// <summary>
        /// Interface contains CRUD functions to exchange information about account between the service and the client
        /// </summary>
        public static IAccountRequest Account { get; }
        /// <summary>
        /// Interface contains CRUD functions to exchange information about organizations between the service and the client
        /// </summary>
        public static IOrganizationRequest Organization { get; }
        /// <summary>
        /// Interface contains CRUD functions to exchange information about application between the service and the client
        /// </summary>
        public static IApplicationRequest Application { get; }
        /// <summary>
        /// Interface allows you to get informations about available regions
        /// </summary>
        public static IRegionRequest Regions { get; }

        static ServiceRequest()
        {
            Account = new AccountRequest("/api/account/", RequestSender.SendRequestAsync);
            Organization = new OrganizationRequest("/api/organization/", RequestSender.SendRequestAsync);
            Application = new ApplicationRequest("/api/application/", RequestSender.SendRequestAsync);
            Regions = new RegionRequest("/api/region/", RequestSender.SendRequestAsync);
        }
    }
}