﻿namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        private struct RequestHandler
        {
            public bool isSuccess { get; set; }
            public string error { get; set; }
            public string text { get; set; }
        }
    }
}