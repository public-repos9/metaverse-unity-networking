﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine.Networking;

namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        private sealed partial class OrganizationRequest : SendRequest, IOrganizationRequest
        {
            private string Controller { get; }

            public IOrganization[] Organizations { get; private set; }

            public OrganizationRequest(string controller, Func<string, string, object, string, Task<RequestHandler>> sendRequest) : base(sendRequest)
            {
                this.Controller = controller;
            }

            /// <summary>
            /// Get All Organizations
            /// </summary>
            /// <returns>All organizations where user is a member.</returns>
            public async Task<IOrganization[]> GetAll()
            {
                string url = string.Concat(URL, Controller);
                string method = UnityWebRequest.kHttpVerbGET;

                RequestHandler result = await Request.Invoke(url, method, null, Account.Token);

                if (result.isSuccess)
                {
                    MUNLogs.ShowLog("Get Organizations Success!".WithColorSuccess());
                    Organizations = JsonRoot<Organization[]>.FromJson(result.text).root;
                }
                else
                {
                    Organizations = null;
                    MUNLogs.ShowError(result.error);
                }

                return Organizations;
            }

            public async Task<IOrganization> LoadOrganizationMembers(IOrganization organization)
            {
                string url = string.Concat(URL, Controller, $"get-members/{organization.Id}");
                string method = UnityWebRequest.kHttpVerbGET;

                RequestHandler result = await Request.Invoke(url, method, null, Account.Token);

                if (result.isSuccess)
                {
                    MUNLogs.ShowLog("Get Organizations Success!".WithColorSuccess());
                    string[] members = JsonRoot<string[]>.FromJson(result.text).root;
                    organization.UpdateMembers(members);
                }
                else
                {
                    MUNLogs.ShowError(result.error);
                }

                return organization;
            }

            /// <summary>
            /// Create new organizations
            /// </summary>
            /// <param name="organizationName">Unique organization name</param>
            /// <returns>True if success</returns>
            public async Task<bool> Create(string organizationName, Action<IResultErrorHandle> onError)
            {
                string url = string.Concat(URL, Controller, "create");
                string method = UnityWebRequest.kHttpVerbPOST;
                CreateOrganization organization = new CreateOrganization()
                {
                    Name = organizationName
                };

                RequestHandler result = await Request.Invoke(url, method, organization, Account.Token);

                if (result.isSuccess)
                {
                    MUNLogs.ShowLog("Create Organizations Success!".WithColorSuccess());
                    return true;
                }
                else
                {
                    ResultErrorHandle errorResult = ResultErrorHandle.FromRequestHandler(result);
                    MUNLogs.ShowError(errorResult.ToString());

                    onError?.Invoke(errorResult);
                    return false;
                }
            }

            /// <summary>
            /// Delete organization (only if owner of this organization).
            /// </summary>
            /// <param name="organizationId">Id of organization to delete</param>
            public async Task<bool> Delete(int organizationId, Action<IResultErrorHandle> onError)
            {
                string url = string.Concat(URL, Controller, $"delete/{organizationId}");
                string method = UnityWebRequest.kHttpVerbDELETE;

                RequestHandler result = await Request.Invoke(url, method, null, Account.Token);

                if (result.isSuccess)
                {
                    MUNLogs.ShowLog("Delete Organizations Success!".WithColorSuccess());
                    Organizations = Organizations.Where(x => x.Id != organizationId).ToArray();
                    return true;
                }
                else
                {
                    ResultErrorHandle errorResult = ResultErrorHandle.FromRequestHandler(result);
                    MUNLogs.ShowError(errorResult.ToString());

                    onError?.Invoke(errorResult);
                    return false;
                }
            }

            public async Task<bool> AddMemberToOrganization(string username, int organizationId, Action<IResultErrorHandle> onError)
            {
                string url = string.Concat(URL, Controller, "add-member");
                string method = UnityWebRequest.kHttpVerbPOST;
                AddMember addMember = new AddMember()
                {
                    Username = username,
                    OrganizationId = organizationId
                };

                RequestHandler result = await Request.Invoke(url, method, addMember, Account.Token);

                if (result.isSuccess)
                {
                    MUNLogs.ShowLog("Add member to Organizations Success!".WithColorSuccess());

                    IOrganization organization = Organizations.First(x => x.Id == organizationId);

                    List<string> newMembers = organization.Members.ToList();
                    newMembers.Add(addMember.Username);

                    organization.UpdateMembers(newMembers.ToArray());

                    return true;
                }
                else
                {
                    ResultErrorHandle errorResult = ResultErrorHandle.FromRequestHandler(result);
                    MUNLogs.ShowError(errorResult.ToString());

                    onError?.Invoke(errorResult);
                    return false;
                }
            }

            public async Task<bool> RemoveMemberFromOrganization(string username, int organizationId)
            {
                string url = string.Concat(URL, Controller, "remove-member");
                string method = UnityWebRequest.kHttpVerbPOST;
                AddMember removeMember = new AddMember()
                {
                    Username = username,
                    OrganizationId = organizationId
                };

                RequestHandler result = await Request.Invoke(url, method, removeMember, Account.Token);

                if (result.isSuccess)
                {
                    MUNLogs.ShowLog("Remove member from Organizations Success!".WithColorSuccess());

                    IOrganization organization = Organizations.First(x => x.Id == organizationId);

                    List<string> newMembers = organization.Members.ToList();
                    newMembers.Remove(removeMember.Username);

                    organization.UpdateMembers(newMembers.ToArray());

                    return true;
                }
                else
                {
                    ResultErrorHandle errorResult = ResultErrorHandle.FromRequestHandler(result);
                    MUNLogs.ShowError(errorResult.ToString());
                    return false;
                }
            }

            public async Task<bool> RemoveFromOrganization(int organizationId, Action<IResultErrorHandle> onError)
            {
                string url = string.Concat(URL, Controller, $"remove-from-organization/{organizationId}");
                string method = UnityWebRequest.kHttpVerbGET;

                RequestHandler result = await Request.Invoke(url, method, null, Account.Token);

                if (result.isSuccess)
                {
                    MUNLogs.ShowLog("Remove from Organizations Success!".WithColorSuccess());
                    Organizations = Organizations.Where(x => x.Id != organizationId).ToArray();
                    return true;
                }
                else
                {
                    ResultErrorHandle errorResult = ResultErrorHandle.FromRequestHandler(result);
                    MUNLogs.ShowError(errorResult.ToString());

                    onError?.Invoke(errorResult);
                    return false;
                }
            }

            public void ClearOrganizations()
            {
                Organizations = null;
            }
        }
    }
}