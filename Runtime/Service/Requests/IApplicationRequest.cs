﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        /// <summary>
        /// Interface contains CRUD functions to exchange information about application between the service and the client
        /// </summary>
        public interface IApplicationRequest
        {
            /// <summary>
            /// Array of applications in organizations to which the currently logged in user belongs
            /// </summary>
            IApplication[] Applications { get; }

            /// <summary>
            /// Sending a request to the website for a attempt to create new application
            /// </summary>
            /// <param name="createApplication">Information about the application</param>
            /// <returns>True if the organization is successful otherwise false</returns>
            Task<bool> Create(CreateApplication createApplication, Action<IResultErrorHandle> errors);

            /// <summary>
            /// Sending a request to the website for a attempt to delete application
            /// </summary>
            /// <param name="appId">Application Id that belongs to the organization you are a member of</param>
            /// <returns></returns>
            Task<bool> Delete(string appId, Action<IResultErrorHandle> onErrors);

            /// <summary>
            /// Sending a request to the website for a attempt to get all applications in organization that user is member
            /// </summary>
            /// <param name="organizationId">Organization Id that you are a member</param>
            /// <returns>Collection of applications</returns>
            Task<IEnumerable<IApplication>> GetAll(int organizationId);

            /// <summary>
            /// Sending into service query abount application that contains appId equals function parameter
            /// </summary>
            /// <param name="appId">Unique application id</param>
            /// <returns>Returns data about application from the service</returns>
            Task<IApplication> Get(string appId);

            /// <summary>
            /// Sending a request to the website for a attempt to update application
            /// </summary>
            /// <param name="updateApplication">Details about the application that you want to change</param>
            /// <returns>True if the organization is successful otherwise false</returns>
            Task<bool> Update(UpdateApplication updateApplication, Action<IResultErrorHandle> errors);

            /// <summary>
            /// Clear array of <see cref="Applications"/>
            /// </summary>
            void ClearApplications();

            Task<bool> Test(string value, Action<IResultErrorHandle> errors);
        }
    }
}