﻿namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        public interface IOrganization
        {
            int Id { get; }
            string Name { get; }
            bool IAmOwner { get; }
            int MembersCount { get; }
            string[] Members { get; }

            void UpdateMembers(string[] members);
        }
    }
}