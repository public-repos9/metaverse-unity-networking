using System;

namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        private sealed partial class AccountRequest
        {
            [Serializable]
            public class RegisterUser
            {
                public string Username;
                public string Email;
                public string Password;
                public string ConfirmPassword;
            }
        }
    }
}