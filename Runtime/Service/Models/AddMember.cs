﻿using System;

namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        private sealed partial class OrganizationRequest
        {
            [Serializable]
            private class AddMember
            {
                public string Username;
                public int OrganizationId;
            }
        }
    }
}