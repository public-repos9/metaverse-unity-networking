﻿using System;

namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        private sealed partial class OrganizationRequest
        {
            [Serializable]
            private class Organization : IOrganization
            {
                public int id;
                public string name;
                public bool iAmOwner;
                public int membersCount;
                public string[] members;

                public int Id => id;
                public string Name => name;
                public bool IAmOwner => iAmOwner;

                public int MembersCount => membersCount;
                public string[] Members => members;

                public void UpdateMembers(string[] members)
                {
                    if (membersCount > 1 && members[0] != Account.Username)
                    {
                        string memberAtZero = members[0];
                        int myIndex = Array.IndexOf(members, Account.Username);

                        members[myIndex] = memberAtZero;
                        members[0] = Account.Username;
                    }

                    this.members = members;
                    membersCount = members.Length;
                }
            }
        }
    }
}