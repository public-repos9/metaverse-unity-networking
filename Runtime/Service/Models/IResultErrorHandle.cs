﻿using System.Collections.Generic;

namespace MUN.Service
{
    public interface IResultErrorHandle
    {
        Dictionary<string, string> Errors { get; }
        int Status { get; }
        string Title { get; }
    }
}