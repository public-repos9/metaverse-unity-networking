﻿namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        private sealed partial class AccountRequest
        {
            [System.Serializable]
            private class UpdateUser
            {
                public string username;
                public string email;
                public string currentPassword;
                public string password;
                public string confirmPassword;

                public UpdateUser(string username, string email, string currentPassword, string password, string confirmPassword)
                {
                    this.username = username;
                    this.email = email;
                    this.currentPassword = currentPassword;
                    this.password = password;
                    this.confirmPassword = confirmPassword;
                }
            }
        }
    }
}