﻿using System;

namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        [Serializable]
        public class CreateApplication
        {
            public string name;
            public uint maxPlayers;
            public byte tickRate;
            public int organizationId;
            public string[] regionNames;
            public bool useVoiceChat;
        }
    }
}