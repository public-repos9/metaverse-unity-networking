using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MUN.Client
{
    /// <summary>
    /// System improving the scene change mechanism in MUN
    /// </summary>
    public class MUNSceneManager : MonoBehaviour
    {
        public static event Action onSceneChangedEvent;

        private static MUNSceneManager instance;
        private static MUNSceneManager Instance
        {
            get
            {
                if (instance is null)
                {
                    instance = FindObjectOfType<MUNSceneManager>();
                    if (instance is null)
                    {
                        instance = new GameObject("CustomSceneManager").AddComponent<MUNSceneManager>();
                    }
                }

                return instance;
            }
        }
        private bool InChangeScene { get; set; }

        /// <summary>
        /// True if scene is changing.
        /// </summary>
        internal static bool IsSceneChanging => Instance.InChangeScene;

        private void OnDisable()
        {
            StopAllCoroutines();
        }

        /// <summary>
        /// Load the Scene
        /// </summary>
        /// <param name="sceneBuildIndex">Index of the scene in the build settings to load.</param>
        /// <param name="onSceneLoaded">action invoked after scene loaded</param>
        public static void LoadScene(int sceneBuildIndex, Action onSceneLoaded)
        {
            Instance.LoadSceneAsync(sceneBuildIndex, onSceneLoaded);
        }

        /// <summary>
        /// Load the Scene
        /// </summary>
        /// <param name="sceneName">Scene name in the build settings to load.</param>
        /// <param name="onSceneLoaded">action invoked after scene loaded</param>
        public static void LoadScene(string sceneName, Action onSceneLoaded)
        {
            int sceneBuildIndex = SceneUtility.GetBuildIndexByScenePath(sceneName);
            LoadScene(sceneBuildIndex, onSceneLoaded);
        }

        private void LoadSceneAsync(int sceneBuildIndex, Action onSceneLoaded)
        {
            if (InChangeScene)
            {
                MUNLogs.ShowWarning("The scene has been changing now");
                return;
            }

            if (MUNLoop.IsQuit) return;
            StartCoroutine(LoadSceneEnumerator(sceneBuildIndex, LoadSceneMode.Single, onSceneLoaded));
        }

        private IEnumerator LoadSceneEnumerator(int sceneBuildIndex, LoadSceneMode loadSceneMode, Action onSceneLoaded)
        {
            InChangeScene = true;

            AsyncOperation aO = SceneManager.LoadSceneAsync(sceneBuildIndex, loadSceneMode);
            aO.completed += (asyncOperation) =>
            {
                InChangeScene = false;

                onSceneLoaded?.Invoke();
                onSceneChangedEvent?.Invoke();
            };

            while (!aO.isDone)
                yield return null;
        }
    }
}