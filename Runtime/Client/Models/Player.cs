using MUN.Client.Extensions;
using MUN.Client.VoiceChat;
using System;
using System.Linq;
using UnityEngine;
using static MUN.Client.MUNNetwork;

namespace MUN.Client
{
    /// <summary>
    /// Represents the model of the local player and every other player in the room
    /// (if the local player is in the room).
    /// </summary>
    /// <remarks>
    /// Each player has a unique identifier (<see cref="Id"/>) assigned after joining the MasterServer
    /// </remarks>
    [Serializable]
    public class Player
    {
        private string name;
        private MUNSpeaker speaker;

        /// <summary>
        /// Unique player Id
        /// </summary>
        [field: SerializeField]
        public int Id { get; set; }
        /// <summary>
        /// Player name
        /// </summary>
        public string Name
        {
            get => name;
            set
            {
                if (string.IsNullOrEmpty(value)
                    || value.Equals(name)) return;

                name = value;
                if (IsLocal) MUNNetwork.Client.ChangePlayerProperties(Id, new LightHashTable() { { PlayerProperties.Name, name } });
            }
        }
        /// <summary>
        /// True if this player is your local player.
        /// </summary>
        public bool IsLocal { get; }
        /// <summary>
        /// True if this player is room owner
        /// </summary>
        public bool IsRoomOwner
        {
            get
            {
                if (Room is null) return false;
                return Room.OwnerId == Id;
            }
        }
        /// <summary>
        /// A reference to the room that this player is in.
        /// </summary>
        protected internal Room Room { get; set; }
        /// <summary>
        /// Custom player properties
        /// </summary>
        public LightHashTable Properties { get; set; }

        public IAudioOutput Speaker
        {
            get
            {
                if (speaker == null)
                    speaker = GameObject.FindObjectsOfType<MUNSpeaker>().FirstOrDefault(x => x.OwnerId.Equals(Id));

                return speaker;
            }
        }

        protected internal Player(int id, LightHashTable properties)
        {
            Id = id;
            Properties = new LightHashTable();
            SetupProperties(properties);
        }

        protected internal Player(string name, int id = -1, bool isLocal = false, LightHashTable properties = null)
        {
            Name = name;
            Id = id;
            IsLocal = isLocal;

            Properties = new LightHashTable();
            if (properties != null) SetupProperties(properties);
        }

        /// <summary>
        /// A method that allows you to set custom properties for a this player.
        /// </summary>
        /// <param name="properties">Custom player properties</param>
        public void SetCustomProperties(LightHashTable properties)
        {
            if (properties == null || properties.Count == 0)
                return;

            if (this.Id != LocalPlayer.Id && CurrentRoom.OwnerId != LocalPlayer.Id)
            {
                MUNLogs.ShowError("Only PlayerOwner or RoomOwner can update player properties.");
                return;
            }

            LightHashTable customProp = properties.ToStringKeys();
            MUNNetwork.Client.ChangePlayerProperties(this.Id, customProp);
        }

        /// <summary>
        /// A method that allows a room owner to appoint a new owner for a room.
        /// </summary>
        public void SetAsRoomOwner()
        {
            if (!LocalPlayer.IsRoomOwner) return;
            Room.ChangeOwner(this);
        }

        /// <summary>
        /// A method that kicks the player out of the room.
        /// Functionality available only to the room owner.
        /// </summary>
        public void KickPlayer()
        {
            if (this.Id != LocalPlayer.Id && CurrentRoom.OwnerId != LocalPlayer.Id)
                return;

            MUNNetwork.Client.KickPlayer(Id);
        }

        /// <summary>
        /// A method that allows you to assign custom player properties
        /// </summary>
        /// <param name="properties">Custom player properties</param>
        protected internal virtual void SetupProperties(LightHashTable properties)
        {
            if (properties is null)
            {
                MUNLogs.ShowException(new ArgumentNullException("Player properties are null."));
                return;
            }
            if (properties.Count == 0)
            {
                MUNLogs.ShowException(new ArgumentException("Player properties are empty"));
                return;
            }
            if (Properties.Equals(properties)) return;

            if (properties.ContainsKey(PlayerProperties.Id.ToString()))
            {
                Id = properties.TryGetValue<int>(PlayerProperties.Id.ToString());
            }

            if (properties.ContainsKey(PlayerProperties.Name.ToString()))
            {
                string propertyName = properties.TryGetValue<string>(PlayerProperties.Name.ToString());

                if (!string.IsNullOrEmpty(propertyName) && !propertyName.Equals(Name))
                {
                    name = propertyName;
                }
            }

            Properties.MergeStrings(properties);
            Properties.RemoveNullValues();
        }
    }
}