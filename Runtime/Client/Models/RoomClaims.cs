﻿namespace MUN.Client
{
    public partial class Room
    {
        public static RoomClaimsBuilder Claims => new RoomClaimsBuilder();

        /// <summary>
        /// The class used when creating a new room.
        /// It contains the basic properties of the room, and also allows you to enter custom properties.
        /// </summary>
        public class RoomClaims
        {
            /// <summary>
            /// A flag that determines whether the room will be open to other players who want to join the room.
            /// A locked room will not be available in the list of available rooms in OnRoomListUpdated.
            /// </summary>
            public bool IsOpen { get; set; }
            /// <summary>
            /// A flag that determines whether a given room will be displayed in the list of available rooms in OnRoomListUpdated.
            /// Players can still join a room by entering its Id.
            /// </summary>
            public bool IsVisible { get; set; }
            /// <summary>
            /// Password for the room to be created. Leave blank if you do not want to set a password.
            /// </summary>
            /// <remarks>
            /// The password is saved on the server side and other clients do not keep it with them.
            /// When you try to join the room, the server takes care of the password verification.
            /// </remarks>
            public string Password { get; set; }
            /// <summary>
            /// The maximum number of players allowed to join a room.
            /// </summary>
            public byte MaxPlayers { get; set; }
            /// <summary>
            /// Custom room properties
            /// </summary>
            public LightHashTable Properties { get; set; }

            /// <summary>
            /// Default constructor for setting basic room parameters
            /// </summary>
            public RoomClaims()
            {
                IsOpen = true;
                IsVisible = true;
                Password = string.Empty;
                MaxPlayers = 4;
                Properties = new LightHashTable();
            }
        }

        public class RoomClaimsBuilder
        {
            private RoomClaims _target;

            #region Constructors

            public RoomClaimsBuilder(RoomClaims target)
            {
                if (target is null)
                    _target = new RoomClaims();
                else
                    _target = target;
            }

            public RoomClaimsBuilder() : this(new RoomClaims())
            {
            }

            #endregion Constructors

            #region Builder Methods

            public RoomClaimsBuilder IsOpen(bool value)
            {
                _target.IsOpen = value;
                return this;
            }

            public RoomClaimsBuilder IsVisible(bool value)
            {
                _target.IsVisible = value;
                return this;
            }

            public RoomClaimsBuilder WithPassword(string value)
            {
                _target.Password = value;
                return this;
            }

            public RoomClaimsBuilder WithMaxPlayers(byte value)
            {
                _target.MaxPlayers = value;
                return this;
            }

            public RoomClaimsBuilder WithNewCustomProperty(object key, object value)
            {
                _target.Properties.Add(key, value);
                return this;
            }

            #endregion Builder Methods

            #region Builder

            private RoomClaims Build() => _target;

            public static implicit operator RoomClaims(RoomClaimsBuilder builder)
            {
                return builder.Build();
            }

            #endregion Builder
        }
    }
}