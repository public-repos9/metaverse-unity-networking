using MUN.Client.Extensions;
using static MUN.Client.MUNNetwork;

namespace MUN.Client
{
    /// <summary>
    /// The class has detailed information about the room the player was connecting in.
    /// The list of rooms is also represented by ten models.
    /// </summary>
    /// <remarks>
    /// A RoomData contains a reference to the <see cref="Player"/>s in it,
    /// as well as some default properties, such as:
    /// <list type="bullet">
    /// <item>Room name</item>
    /// <item>Unique RoomId</item>
    /// <item>Who is a RoomOwner</item>
    /// <item>Is the room open</item>
    /// <item>Whether the room is visible</item>
    /// <item>Does the room have looby</item>
    /// <item>Does the room have a password</item>
    /// <item>How many players are in the room</item>
    /// </list>
    ///
    /// And also has access to custom properties that can be set when creating a room,
    /// as well as after its creation using the <see cref="SetupProperties(LightHashTable)"/> function.
    /// </remarks>
    public class RoomData
    {
        /// <summary>
        /// Custom room name
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// A flag for the unique room identifier property.
        /// Using RoomId, we can connect to the room.
        /// </summary>
        public string RoomId { get; private set; }
        /// <summary>
        /// The flag indicating whether the room has a password. The password is saved on the server side.
        /// Clients only get information whether a given room has a password.
        /// The password is also verified on the server side.
        /// </summary>
        public int OwnerId { get; protected set; }
        /// <summary>
        /// A flag that determines whether the room will be open to other players who want to join the room.
        /// A locked room will not be available in the list of available rooms in OnRoomListUpdated.
        /// </summary>
        public bool IsOpen { get; protected set; }
        /// <summary>
        /// A flag that determines whether a given room will be displayed in the list of available rooms in OnRoomListUpdated.
        /// Players can still join a room by entering its Id.
        /// </summary>
        public bool IsVisible { get; protected set; }
        /// <summary>
        /// The flag indicating whether the room has a password. The password is saved on the server side.
        /// Clients only get information whether a given room has a password.
        /// The password is also verified on the server side.
        /// </summary>
        public bool HasPassword { get; protected set; }
        /// <summary>
        /// The maximum number of players allowed to join a room.
        /// </summary>
        public byte MaxPlayers { get; protected set; }
        /// <summary>
        /// A flag showing the current number of players in the room.
        /// </summary>
        public byte PlayersCount { get; private set; }
        /// <summary>
        /// Custom room properties
        /// </summary>
        public LightHashTable Properties { get; protected set; }

        protected internal RoomData(string roomName, LightHashTable properties)
        {
            Name = roomName;
            Properties = new LightHashTable();
            SetupProperties(properties);
        }

        public override bool Equals(object other)
        {
            RoomData otherData = other as RoomData;
            return otherData != null && RoomId.Equals(otherData.RoomId);
        }

        public override string ToString()
        {
            return $"<b>Id:</b>{RoomId} ,<b>Room</b>: {Name}, IsVisible: {IsVisible}, IsOpen: {IsOpen}, HasPassword: {HasPassword}, Max Players: {MaxPlayers}";
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// A method that allows you to assign custom room properties
        /// </summary>
        /// <param name="properties">Custom room properties</param>
        protected internal virtual void SetupProperties(LightHashTable properties)
        {
            if (properties is null || properties.Count == 0) return;
            if (Properties.Equals(properties)) return;

            if (properties.ContainsKey(RoomProperties.RoomId))
            {
                RoomId = (string)properties[RoomProperties.RoomId];
            }

            if (properties.ContainsKey(RoomProperties.OwnerId))
            {
                OwnerId = (int)properties[RoomProperties.OwnerId];
            }

            if (properties.ContainsKey(RoomProperties.IsOpen))
            {
                IsOpen = (bool)properties[RoomProperties.IsOpen];
            }

            if (properties.ContainsKey(RoomProperties.IsVisible))
            {
                IsVisible = (bool)properties[RoomProperties.IsVisible];
            }

            if (properties.ContainsKey(RoomProperties.MaxPlayers))
            {
                MaxPlayers = (byte)properties[RoomProperties.MaxPlayers];
            }

            if (properties.ContainsKey(RoomProperties.PlayersCount))
            {
                PlayersCount = (byte)properties[RoomProperties.PlayersCount];
            }

            if (properties.ContainsKey(RoomProperties.HasPassword))
            {
                HasPassword = (bool)properties[RoomProperties.HasPassword];
            }

            Properties.MergeStrings(properties);
            Properties.RemoveNullValues();
        }
    }
}