using MUN.Client;
using System.Threading.Tasks;
using UnityEngine;
using static MUN.Client.MUNNetwork;

namespace MUN
{
    [RequireComponent(typeof(MUNView))]
    public class UPD_Initializer : MonoBehaviourMUN
    {
        private async void Start()
        {
            if (!MUNView.IsMine)
                return;

            DontDestroyOnLoad(this);
            await TryToSend();
        }

        private async Task TryToSend()
        {
            await Task.Delay(1000);

            if (MUNNetwork.Status >= MUNNetwork.ClientStatus.ConnectedToMasterServer)
            {
                MUNLogs.ShowLog("RPC_Initialize Start!");
                MUNView.SendRPC(nameof(RPC_Initialize), RPCTargets.AllInRoomCache);
            }
        }

        [MunRPC]
        public void RPC_Initialize()
        {
            if (MUNView.IsMine)
                MUNLogs.ShowLog("RPC_Initialize Successful!");

            Destroy(gameObject);
        }
    }
}