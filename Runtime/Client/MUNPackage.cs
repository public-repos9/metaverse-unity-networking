﻿using System.Collections.Generic;
using UnityEngine;

namespace MUN.Client
{
    /// <summary>
    /// A class that enables serialization of the MUNView
    /// </summary>
    public class MUNPackage
    {
        private readonly Packet writeData;
        private Packet readData;

        private readonly List<object> writeDataItemsHelper;

        /// <summary>
        /// If true, that package is set to write data
        /// </summary>
        public bool IsWriting { get; private set; }

        /// <summary>
        /// If true, that package is set to read data
        /// </summary>
        public bool IsReading => !IsWriting;

        /// <summary>
        /// Count of items in package
        /// </summary>
        public int Count => IsWriting ? writeData.Length : readData.Length;

        /// <summary>
        /// Returns write/read data as array.
        /// Write - if IsWriting (true)
        /// Read - if IsReading (true)
        /// </summary>
        internal Packet GetData => IsWriting ? writeData : readData;

        internal object[] GetWriteDataItemsHelper => writeDataItemsHelper.ToArray();

        private MUNPackage(bool isWrite)
        {
            writeData = new Packet();
            readData = new Packet();
            writeDataItemsHelper = new List<object>();

            IsWriting = isWrite;
        }

        /// <summary>
        /// Creates an object that enables data to be written
        /// </summary>
        protected internal static MUNPackage MUNPackageWrite()
        {
            return new MUNPackage(true);
        }

        /// <summary>
        /// Creates an object that enables data to be read
        /// </summary>
        /// <returns></returns>
        protected internal static MUNPackage MUNPackageRead()
        {
            return new MUNPackage(false);
        }

        /// <summary>
        /// Assigns readable data to an object
        /// </summary>
        /// <param name="data">data set to read</param>
        internal void SetReadData(Packet data)
        {
            IsWriting = false;
            readData = data;
            writeDataItemsHelper.Clear();
        }

        /// <summary>
        /// Sets an object (clears data) to be read
        /// </summary>
        internal void SetWriteData()
        {
            IsWriting = true;
            writeData.Dispose();
            writeDataItemsHelper.Clear();
        }

        #region RECIEVE DATA

        /// <summary>
        /// Reads a <code>byte</code> from the MUNPackage
        /// </summary>
        public byte ReadByte()
        {
            if (!CheckCanRead()) return default;
            return readData.ReadByte();
        }

        /// <summary>
        /// Reads a <code>byte[]</code> from the MUNPackage
        /// </summary>
        public byte[] ReadBytes()
        {
            if (!CheckCanRead()) return default;
            return readData.ReadBytes(readData.ReadInt());
        }

        /// <summary>
        /// Reads a <code>short</code> from the MUNPackage
        /// </summary>
        public short ReadShort()
        {
            if (!CheckCanRead()) return default;
            return readData.ReadShort();
        }

        /// <summary>
        /// Reads a <code>int</code> from the MUNPackage
        /// </summary>
        public int ReadInt()
        {
            if (!CheckCanRead()) return default;
            return readData.ReadInt();
        }

        /// <summary>
        /// Reads a <code>long</code> from the MUNPackage
        /// </summary>
        public long ReadLong()
        {
            if (!CheckCanRead()) return default;
            return readData.ReadLong();
        }

        /// <summary>
        /// Reads a <code>float</code> from the MUNPackage
        /// </summary>
        public float ReadFloat()
        {
            if (!CheckCanRead()) return default;
            return readData.ReadFloat();
        }

        /// <summary>
        /// Reads a <code>bool</code> from the MUNPackage
        /// </summary>
        public bool ReadBool()
        {
            if (!CheckCanRead()) return default;
            return readData.ReadBool();
        }

        /// <summary>
        /// Reads a <code>string</code> from the MUNPackage
        /// </summary>
        public string ReadString()
        {
            if (!CheckCanRead()) return default;
            return readData.ReadString();
        }

        /// <summary>
        /// Reads a <code>Vector2</code> from the MUNPackage
        /// </summary>
        public Vector2 ReadVector2()
        {
            if (!CheckCanRead()) return default;
            return readData.ReadVector2();
        }

        /// <summary>
        /// Reads a <code>Vector3</code> from the MUNPackage
        /// </summary>
        public Vector3 ReadVector3()
        {
            if (!CheckCanRead()) return default;
            return readData.ReadVector3();
        }

        /// <summary>
        /// Reads a <code>Quaternion</code> from the MUNPackage
        /// </summary>
        public Quaternion ReadQuaternion()
        {
            if (!CheckCanRead()) return default;
            return readData.ReadQuaternion();
        }

        private bool CheckCanRead()
        {
            if (IsWriting)
            {
                MUNLogs.ShowError("MUNPackage is writing. Can not read data.");
                return false;
            }

            return true;
        }

        #endregion RECIEVE DATA

        #region SEND DATA

        /// <summary>
        /// Adds a <code>byte</code> value to the MUNPackage
        /// </summary>
        public void Write(byte value)
        {
            if (!CheckCanWrite()) return;
            writeData.Write(value);
            writeDataItemsHelper.Add(value);
        }

        /// <summary>
        /// Adds a <code>byte[]</code> value to the MUNPackage
        /// </summary>
        public void Write(byte[] value)
        {
            if (!CheckCanWrite()) return;
            writeData.Write(value.Length);
            writeData.Write(value);
            writeDataItemsHelper.Add(value);
        }

        /// <summary>
        /// Adds a <code>short</code> value to the MUNPackage
        /// </summary>
        public void Write(short value)
        {
            if (!CheckCanWrite()) return;
            writeData.Write(value);
            writeDataItemsHelper.Add(value);
        }

        /// <summary>
        /// Adds a <code>int</code> value to the MUNPackage
        /// </summary>
        public void Write(int value)
        {
            if (!CheckCanWrite()) return;
            writeData.Write(value);
            writeDataItemsHelper.Add(value);
        }

        /// <summary>
        /// Adds a <code>long</code> value to the MUNPackage
        /// </summary>
        public void Write(long value)
        {
            if (!CheckCanWrite()) return;
            writeData.Write(value);
            writeDataItemsHelper.Add(value);
        }

        /// <summary>
        /// Adds a <code>float</code> value to the MUNPackage
        /// </summary>
        public void Write(float value)
        {
            if (!CheckCanWrite()) return;
            writeData.Write(value);
            writeDataItemsHelper.Add(value);
        }

        /// <summary>
        /// Adds a <code>bool</code> value to the MUNPackage
        /// </summary>
        public void Write(bool value)
        {
            if (!CheckCanWrite()) return;
            writeData.Write(value);
            writeDataItemsHelper.Add(value);
        }

        /// <summary>
        /// Adds a <code>string</code> value to the MUNPackage
        /// </summary>
        public void Write(string value)
        {
            if (!CheckCanWrite()) return;
            writeData.Write(value);
            writeDataItemsHelper.Add(value);
        }

        /// <summary>
        /// Adds a <code>Vector2</code> value to the MUNPackage
        /// </summary>
        public void Write(Vector2 value)
        {
            if (!CheckCanWrite()) return;
            writeData.Write(value);
            writeDataItemsHelper.Add(value);
        }

        /// <summary>
        /// Adds a <code>Vector3</code> value to the MUNPackage
        /// </summary>
        public void Write(Vector3 value)
        {
            if (!CheckCanWrite()) return;
            writeData.Write(value);
            writeDataItemsHelper.Add(value);
        }

        /// <summary>
        /// Adds a <code>Quaternion</code> value to the MUNPackage
        /// </summary>
        public void Write(Quaternion value)
        {
            if (!CheckCanWrite()) return;
            writeData.Write(value);
            writeDataItemsHelper.Add(value);
        }

        private bool CheckCanWrite()
        {
            if (IsReading)
            {
                MUNLogs.ShowError("MUNPackage is reading. Can not write data");
                return false;
            }

            return true;
        }

        #endregion SEND DATA
    }
}