﻿using MUN.Client.Interfaces;

namespace MUN.Client
{
    public static partial class MUNNetwork
    {
        internal partial class MUNClient
        {
            private sealed class MUNViewContainer : MUNContainerBase<IMUNView>, IMUNView
            {
                public MUNViewContainer(MUNClient client) : base(client)
                {
                }

                public void OnMUNViewCreated(MUNView newMUNView)
                {
                    client.UpdateCallbacks();
                    foreach (IMUNView target in this)
                    {
                        target.OnMUNViewCreated(newMUNView);
                    }
                }

                public void OnMUNViewOwnerChanged(MUNView munView, Player newOwner)
                {
                    client.UpdateCallbacks();
                    foreach (IMUNView target in this)
                    {
                        target.OnMUNViewOwnerChanged(munView, newOwner);
                    }
                }

                public void OnPreMUNViewDelete(MUNView munViewToDelete)
                {
                    client.UpdateCallbacks();
                    foreach (IMUNView target in this)
                    {
                        target.OnPreMUNViewDelete(munViewToDelete);
                    }
                }
            }
        }
    }
}