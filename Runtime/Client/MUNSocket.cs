﻿using System;
using System.Collections.Generic;
using System.Net;

namespace MUN.Client
{
    public static partial class MUNNetwork
    {
        internal partial class MUNClient
        {
            private abstract class MUNSocket
            {
                protected readonly MUNClient client;
                protected readonly Dictionary<byte, PacketHandler> packetsHandler;

                protected (IPAddress Ip, int Port) ConnectionString { get; }

                protected internal abstract ProtocolType ProtocolType { get; }

                protected MUNSocket(MUNClient client, Dictionary<byte, PacketHandler> packetsHandler, string ip, int port)
                {
                    this.client = client;
                    this.packetsHandler = packetsHandler;
                    ConnectionString = (IPAddress.Parse(ip), port);
                }

                protected internal abstract void CloseSocket();

                protected internal abstract void SendData(Packet packet);

                protected abstract void Connect();

                protected abstract void Disconnect();

                protected abstract void ReceiveCallback(IAsyncResult result);

                protected abstract bool HandleData(byte[] data);
            }
        }
    }
}