﻿using UnityEngine;
using static MUN.Client.MUNNetwork;

namespace MUN.Client.Extensions
{
    /// <summary>
    /// Extending the float class with additional methods
    /// </summary>
    public static class FloatExtensions
    {
        /// <summary>
        /// A method that compares float to the second object. In the case of type matching, precision is also compared
        /// </summary>
        /// <remarks>
        /// The method used to optimize the data sent to the server. If both values are not changed (or slightly changed) then serialization will not be performed.
        /// The system sends the synchronization after a specified time interval.
        /// </remarks>
        /// <param name="two">Second object you want to check for equality</param>
        /// <returns>True if both objects are of the same type and their difference (distance) is not greater than the specified precision</returns>
        public static bool AreEquals(this float valueA, object two, float precision)
        {
            if (!typeof(float).Equals(two.GetType())) return false;
            float valueB = (float)two;

            return Mathf.Abs(valueB - valueA) < precision;
        }

        /// <summary>
        /// Convert float value to short
        /// </summary>
        public static short ToShort(this float value)
        {
            return (short)(value * Consts.FLOAT_TO_SHORT_CONVERSION_MULTIPLER);
        }

        public static short[] ToShort(this float[] values)
        {
            short[] result = new short[values.Length];

            for (int i = 0; i < result.Length; i++)
            {
                result[i] = values[i].ToShort();
            }

            return result;
        }
    }
}