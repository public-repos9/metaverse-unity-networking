﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MUN.Client.Extensions
{
    /// <summary>
    /// Extending the Transform class with additional methods
    /// </summary>
    public static class TransformExtensions
    {
        /// <summary>
        /// Searches for specific components in a Transform
        /// </summary>
        /// <typeparam name="FindT">The component you are looking for</typeparam>
        /// <param name="includeInactive">Do you also search in excluded facilities?</param>
        public static IEnumerable<Component> FindComponentsInChildren<FindT>(this Transform transform, in bool includeInactive)
        where FindT : class
        {
            List<Component> finded = new List<Component>();
            Type searchType = typeof(FindT);

            Transform[] allTransforms = transform.GetComponentsInChildren<Transform>(includeInactive);

            foreach (Transform child in allTransforms)
            {
                Component[] components = child.GetComponents<Component>();

                foreach (Component component in components)
                {
                    FindT cast = component as FindT;
                    if (!ReferenceEquals(cast, null))
                        finded.Add(cast as Component);
                }
            }

            return finded.Distinct();
        }
    }
}