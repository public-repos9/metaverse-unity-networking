using System.Collections;
using System.Collections.Generic;

namespace MUN.Client.Extensions
{
    /// <summary>
    /// Extending the IDictionary class with additional methods
    /// </summary>
    public static class DictionaryExtensions
    {
        private static readonly List<object> nullValuesListHelper = new List<object>();

        /// <summary>
        /// Concatenates two dictionaries based on the keys.
        /// </summary>
        /// <remarks>
        /// If the second dictionary has a key(string) that exists in the dictionary to be checked, its value will be overwritten.
        /// </remarks>
        /// <param name="dictionary"></param>
        /// <param name="addDictionary"></param>
        public static void MergeStrings(this IDictionary dictionary, IDictionary addDictionary)
        {
            if (addDictionary == null
                || dictionary.Equals(addDictionary))
                return;

            foreach (object key in addDictionary.Keys)
            {
                if (key is string)
                    dictionary[key] = addDictionary[key];
            }
        }

        /// <summary>
        /// Removes null values from an array
        /// </summary>
        public static void RemoveNullValues(this IDictionary dictionary)
        {
            lock (nullValuesListHelper)
            {
                nullValuesListHelper.Clear();

                foreach (DictionaryEntry item in dictionary)
                {
                    if (item.Value is null)
                        nullValuesListHelper.Add(item.Key);
                }

                for (int i = 0; i < nullValuesListHelper.Count; i++)
                {
                    object nullValue = nullValuesListHelper[i];
                    dictionary.Remove(nullValue);
                }
            }
        }

        /// <summary>
        /// Converts dictionary keys to string type
        /// </summary>
        public static LightHashTable ToStringKeys(this IDictionary dictionary)
        {
            LightHashTable hashtable = new LightHashTable();
            if (dictionary != null)
            {
                foreach (object key in dictionary.Keys)
                {
                    if (key is string)
                        hashtable[key] = dictionary[key];
                }
            }

            return hashtable;
        }
    }
}