﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace MUN.Client
{
    public static partial class MUNNetwork
    {
        internal partial class MUNClient
        {
            private class TCP : MUNSocket
            {
                private TcpClient socket;
                private NetworkStream stream;
                private Packet recivedData;
                private byte[] reciveBuffer;

                protected internal override ProtocolType ProtocolType => ProtocolType.TCP;

                public TCP(MUNClient client, Dictionary<byte, PacketHandler> packetsHandler, string ip, int port) : base(client, packetsHandler, ip, port)
                {
                    Connect();
                }

                protected internal override void CloseSocket()
                {
                    socket?.Close();
                }

                protected internal override void SendData(Packet packet)
                {
                    try
                    {
                        if (socket == null)
                        {
                            MUNLogs.ShowException(new NullReferenceException("Socket is null"));
                            return;
                        }
                        packet.WriteLength();
                        if (packet.Length > Consts.MAX_TCP_PACKAGE_SIZE)
                        {
                            MUNLogs.ShowException(new OutOfMemoryException($"Too large a package size {packet.Length} > {Consts.MAX_TCP_PACKAGE_SIZE}"));
                            return;
                        }

                        stream.BeginWrite(packet.ToArray(), 0, packet.Length, null, null);
                    }
                    catch (Exception ex)
                    {
                        MUNLogs.ShowError($"Error sending data to server via {ProtocolType}: {ex.Message}.");
                    }
                }

                protected override void Connect()
                {
                    socket = new TcpClient
                    {
                        ReceiveBufferSize = MAX_DATA_BUFFER_SIZE,
                        SendBufferSize = MAX_DATA_BUFFER_SIZE
                    };

                    reciveBuffer = new byte[MAX_DATA_BUFFER_SIZE];
                    socket.BeginConnect(ConnectionString.Ip, ConnectionString.Port, ConnectCallback, socket);

                    var threadTimeout = new Thread(async () =>
                    {
                        await Task.Delay(TimeSpan.FromSeconds(10));
                        if (ClientStatus == ClientStatus.ConnectingToMasterServer)
                        {
                            MUNLogs.ShowError("Connection Timeout.");
                            Disconnect();
                        }
                    });
                    threadTimeout.IsBackground = true;
                    threadTimeout.Start();
                }

                private void ConnectCallback(IAsyncResult result)
                {
                    socket.EndConnect(result);

                    if (!socket.Connected)
                    {
                        ClientStatus = ClientStatus.Disconnected;
                        return;
                    }

                    stream = socket.GetStream();
                    recivedData = new Packet();
                    stream.BeginRead(reciveBuffer, 0, MAX_DATA_BUFFER_SIZE, ReceiveCallback, null);
                }

                protected override void ReceiveCallback(IAsyncResult result)
                {
                    try
                    {
                        int byteLength = stream.EndRead(result);
                        if (byteLength <= 0)
                        {
                            MUNLogs.ShowException(new Exception("End of data"));
                            return;
                        }

                        byte[] data = new byte[byteLength];
                        Array.Copy(reciveBuffer, data, byteLength);

                        recivedData.Reset(HandleData(data));
                        stream.BeginRead(reciveBuffer, 0, MAX_DATA_BUFFER_SIZE, ReceiveCallback, null);
                    }
                    catch
                    {
                        Disconnect();
                    }
                }

                protected override void Disconnect()
                {
                    client.Disconnect();

                    stream = null;
                    recivedData = null;
                    reciveBuffer = null;
                    socket = null;
                }

                protected override bool HandleData(byte[] data)
                {
                    int packetLength = 0;
                    recivedData.SetBytes(data);

                    if (recivedData.UnreadLength >= 4)
                    {
                        packetLength = recivedData.ReadInt();
                        if (packetLength <= 0) return true;
                    }

                    while (packetLength > 0 && packetLength <= recivedData.UnreadLength)
                    {
                        byte[] packetBytes = recivedData.ReadBytes(packetLength);
                        ThreadManager.ExecuteOnMainThread(() =>
                        {
                            using (Packet packet = new Packet(packetBytes))
                            {
                                byte packetId = packet.ReadByte();
                                if (packetsHandler.TryGetValue(packetId, out PacketHandler handler))
                                {
                                    handler(packet);
                                }
                                else
                                    MUNLogs.ShowError($"Unknown Packet id = ({packetId}) from server.");
                            }
                        });

                        packetLength = 0;
                        if (recivedData.UnreadLength >= 4)
                        {
                            packetLength = recivedData.ReadInt();
                            if (packetLength <= 0) return true;
                        }
                    }

                    return packetLength <= 1;
                }
            }
        }
    }
}