using MUN;
using MUN.Client;
using UnityEngine;

public class MUN_Voice_Debug : MonoBehaviourMUN
{
    [SerializeField, ReadOnly]
    private string voiceChatStatus;

    private void Update()
    {
        voiceChatStatus = MUNNetwork.VoiceStatus.ToString();

        if (Input.GetKeyUp(KeyCode.Alpha1)) LeaveVoiceRoom();
        else if (Input.GetKeyUp(KeyCode.Alpha2)) CreateOrJoinVoiceRoom();
    }

    [ContextMenu("CreateOrJoinVoiceRoom")]
    private void CreateOrJoinVoiceRoom()
    {
        MUNNetwork.CreateOrJoinVoiceRoom();
    }

    [ContextMenu("LeaveVoiceRoom")]
    private void LeaveVoiceRoom()
    {
        MUNNetwork.LeaveVoiceRoom();
    }

    #region Callbacks

    public override void OnConnectedToVoiceChat()
    {
        MUNLogs.ShowLog($"OnConnectedToVoiceChat (Status = {voiceChatStatus})", gameObject);
    }

    public override void OnDisconnectedFromVoiceChat()
    {
        MUNLogs.ShowLog($"OnDisconnectedFromVoiceChat (Status = {voiceChatStatus})", gameObject);
    }

    public override void OnJoinedVoiceRoom()
    {
        MUNLogs.ShowLog($"OnJoinedVoiceRoom: {MUNNetwork.CurrentVoiceRoom.RoomId} (Status = {voiceChatStatus})", gameObject);
    }

    public override void OnLeaveVoiceRoom()
    {
        MUNLogs.ShowLog($"OnLeaveVoiceRoom (Status = {voiceChatStatus})", gameObject);
    }

    public override void OnPlayerJoinedVoiceChat(Player player)
    {
        MUNLogs.ShowLog($"OnPlayerJoinedVoiceChat: {player.Id} (Status = {voiceChatStatus})", gameObject);
    }

    public override void OnPlayerLeftVoiceChat(Player player)
    {
        MUNLogs.ShowLog($"OnPlayerLeftVoiceChat: {player.Id} (Status = {voiceChatStatus})", gameObject);
    }

    #endregion Callbacks
}