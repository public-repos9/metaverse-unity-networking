using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using System.IO;

#if UNITY_EDITOR

using UnityEditor;

#endif

namespace MUN.Client
{
    public class MUNSettings : ScriptableObject
    {
        private static MUNSettings _munSettings;

        [FormerlySerializedAs("rpcs")]
        [SerializeField, SerializeReference]
        private List<string> _rpcs = new List<string>();

        [SerializeField, SerializeReference]
        private string appId = string.Empty;

        [SerializeField, EnumFlags, SerializeReference]
        private MUNLogs.LogTypes logTypes;

        private static bool HasSettingsFile => File.Exists("Assets/EpicVR/Resources/Data/MUNSettings.asset");

        /// <summary>
        /// Application ID.
        /// </summary>
        public string AppID
        {
            get => appId;
            set
            {
                appId = value;
                SaveAsset();
            }
        }

        /// <summary>
        /// The list of RPCs.
        /// </summary>
        public List<string> RPCs
        {
            get => _rpcs;
            private set
            {
                _rpcs = value;
                SaveAsset();
            }
        }

        public MUNLogs.LogTypes LogTypes
        {
            get => logTypes;
            set
            {
                logTypes = value;
                SaveAsset();
            }
        }

        /// <summary>
        /// Object to store MUN application settings.
        /// </summary>
        public static MUNSettings Get()
        {
#if UNITY_EDITOR
            if (!HasSettingsFile)
                CreateSettings();
            else
#endif
                _munSettings = Resources.Load<MUNSettings>("Data/MUNSettings");

            return _munSettings;
        }

        private void SaveAsset()
        {
#if UNITY_EDITOR
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();
#endif
        }

#if UNITY_EDITOR

        public void RefreshRpcs()
        {
            RPCs = GetRpcs();
        }

        public void ClearRpcs()
        {
            RPCs.Clear();
        }

        internal List<string> GetRpcs()
        {
            List<string> list = new List<string>();

            TypeCache.MethodCollection rpcMethods = TypeCache.GetMethodsWithAttribute<MunRPCAttribute>();
            int id = 0;
            foreach (System.Reflection.MethodInfo rpcMethod in rpcMethods)
            {
                if (id < byte.MaxValue)
                {
                    list.Add(rpcMethod.Name);
                }
                else
                {
                    MUNLogs.ShowWarning("RPCs List is full.");
                    break;
                }

                id++;
            }

            list.Sort();
            EditorUtility.SetDirty(this);
            return list;
        }

        private static void CreateSettings()
        {
            var folder = Path.GetFullPath("Assets/EpicVR/Resources/Data");

            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            _munSettings = CreateInstance<MUNSettings>();
            _munSettings.appId = string.Empty;
            _munSettings._rpcs = new List<string>();
            _munSettings.logTypes = MUNLogs.LogTypes.Log | MUNLogs.LogTypes.Warning | MUNLogs.LogTypes.Exception |
                                    MUNLogs.LogTypes.Error;

            AssetDatabase.CreateAsset(_munSettings, "Assets/EpicVR/Resources/Data/MUNSettings.asset");
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

#endif
    }

    public static class LogTypesExntensions
    {
        public static MUNLogs.LogTypes Add(this MUNLogs.LogTypes logTypes, MUNLogs.LogTypes value)
        {
            logTypes |= value;
            return logTypes;
        }

        public static MUNLogs.LogTypes Remove(this MUNLogs.LogTypes logTypes, MUNLogs.LogTypes value)
        {
            logTypes &= ~value;
            return logTypes;
        }
    }
}