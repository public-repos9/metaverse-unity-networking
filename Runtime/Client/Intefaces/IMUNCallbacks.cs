﻿namespace MUN.Client.Interfaces
{
    /// <summary>
    /// The basic interface for all types of callbacks.
    /// The appropriate methods are searched for via this interface.
    /// </summary>
    public interface IMUNCallbacks
    { }
}