namespace MUN.Client.Interfaces
{
    public interface IMUNVoice : IMUNCallbacks
    {
        void OnConnectedToVoiceChat();

        void OnDisconnectedFromVoiceChat();

        void OnJoinedVoiceRoom();

        void OnLeaveVoiceRoom();

        void OnPlayerJoinedVoiceChat(Player player);

        void OnPlayerLeftVoiceChat(Player player);
    }
}