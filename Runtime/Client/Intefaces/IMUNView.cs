﻿namespace MUN.Client.Interfaces
{
    /// <summary>
    /// Interface implementing the methods responsible for managing the MUNView.
    /// </summary>
    public interface IMUNView : IMUNCallbacks
    {
        /// <summary>
        /// Called when any of the clients creates any network object.
        /// </summary>
        /// <param name="newMUNView"></param>
        void OnMUNViewCreated(MUNView newMUNView);

        /// <summary>
        /// Called when any of the clients delete any network object.
        /// </summary>
        /// <remarks>
        /// Only RoomOwner and the property owner can delete the specified network object.
        /// </remarks>
        void OnPreMUNViewDelete(MUNView munViewToDelete);

        /// <summary>
        /// Called when a specific MUNView changes its owner.
        /// </summary>
        /// <remarks>
        /// Only RoomOwner and the owner of the MUNView can set a new owner.
        /// </remarks>
        /// <param name="munView">MUNView That Changed ownership</param>
        /// <param name="newOwner">New owner of the network object</param>
        void OnMUNViewOwnerChanged(MUNView munView, Player newOwner);
    }
}